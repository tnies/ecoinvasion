#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This file contains a generalized Lotka-Volterra model as well as an implementation
of the extended MacArthur's consumer resource model proposed by Marsland  et al III.
"""

import numpy as np
from simulation import simulation_routines
from auxiliary import auxiliary_functions

__author__ = 'Tim Nies'
__credit__='Tim Nies'
__liscence__=''
__vesion__='0.1'
__maintainer__='Tim Nies'

        
class gLV(simulation_routines):
    def __init__(self):
        super(gLV,self).__init__()
        self.params = {'r':[6,4,2],
                      'B': np.array([[-0.05,0.15,-0.20],
                                     [-0.01,-0.027,0.05],
                                     [0.1,-0.1,-0.0148]])
                        }
                      
    def system(self,t,N):
        dN_dt = N*(self.params['r'] + np.sum(self.params['B']*N, axis=1))
        return dN_dt
        
        
        
class Mcrm(simulation_routines,auxiliary_functions):
    def __init__(self):
        super(Mcrm,self).__init__()
        
    def system(self,t,y0):
        
        number_species = self.metaparams['number_species']
        
        N = y0[:number_species]
        R = y0[number_species:]
        
        dN_dt = self.params['g']*N*(np.sum(self.J_growth(R),axis=1)-self.params['m'])
        
        dR_dt = (self.h[self.metaparams['h']](R) 
                -(self.J_in(R)/self.params['w']).T.dot(N) 
                + (self.J_out(R)/self.params['w']).T.dot(N))
    
        
        return np.concatenate((dN_dt, dR_dt), axis=None)
    

    
class Mcrm2(simulation_routines,auxiliary_functions):
    def __init__(self):
        super(Mcrm2,self).__init__()
        
    def system(self,t,y0):
        
        number_species = self.metaparams['number_species']
        
        N = y0[:number_species]
        R = y0[number_species:]
        
        dN_dt = self.params['g']*N*(np.einsum('a,a,ia -> i', (1-self.params['l']), self.params['w'], self.params['C']*R) - self.params['m'])
        
        dR_dt = self.h[self.metaparams['h']](R) - np.einsum('j,ja ->a',N,self.params['C']*R) + np.einsum('j,jb,jab,ba,b -> a', N, self.params['C']*R, self.params['D'], self.params['w'][:,np.newaxis]/self.params['w'], self.params['l'])
        
        return np.concatenate((dN_dt, dR_dt), axis=None)
        
    
    
    
        

        
        
        
    
        
