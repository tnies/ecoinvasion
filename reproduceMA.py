#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This file contains code to reproduce figure 2 of the results published in Mars-
land III et al. 
"""

__author__ = 'Tim Nies'
__credit__='Tim Nies'
__liscence__=''
__vesion__='0.1'
__maintainer__='Tim Nies'


import numpy as np
import seaborn as sns
import pickle
from models import Mcrm
import pandas as pd
from numpy.random import dirichlet
from utils import  getConsumerMatrix, getMetabolismMatrix
import matplotlib.pyplot as plt


#%%

#Form 200 Species 

model0 = Mcrm()
l_invasion = 0.8
n_resources = 100
n_species = 200


model0.change_metaparams({'number_species':n_species,'number_resources':n_resources})
model0.change_metaparams({'h':'renewable', 'sigma': 'type I'})



D0 = getMetabolismMatrix(model0)
C0 = getConsumerMatrix(model0)

p = np.random.choice([0.08,0.02],p=(0.99,0.01),size=100)
D0 = dirichlet(p,100).T



def BinaryRandomMatrix(a,b,p):
    """
    Construct binary random matrix.
    
    a, b = matrix dimensions
    
    p = probability that element equals 1 (otherwise 0)
    
    from Marsland
    """
    r = np.random.rand(a,b)
    m = np.zeros((a,b))
    m[r<p] = 1.0
    return m

C0 = BinaryRandomMatrix(n_species,n_resources,0.1)

#%% set initial values for the ground community

meta_dict = {}
for l in [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]:
    for k in [10,30,50,70,90,110,330,550,770,990,1000]:
        model_dict = {}
        for i in range(10):
            
            model = Mcrm()
            l_invasion = 0.8
            n_resources = 100
            n_species = 100
            
            model.change_metaparams({'number_species':n_species,'number_resources':n_resources})
            model.change_metaparams({'h':'renewable', 'sigma': 'type I'})
            number_species = model.metaparams['number_species']
            number_resources = model.metaparams['number_resources']

            model.params['k'] = np.zeros(number_resources)
            model.params['k'][0] = k
            
            model.params['l'] = np.ones(number_resources)
            model.params['l'] = model.params['l']*l
            
            idx = np.random.choice(range(200),100)
            model.params['D'] = [D0]
            model.params['C'] = C0[idx,:]
            
            model_dict[('model%d' % (i+1))] = model
        
        meta_dict[str((l,np.round(k,2)))] = model_dict
            
#%%

y0_resources = np.zeros(100)
y0_resources[0] = 1e2
y0_species = np.repeat(10e4,100)
y0 = np.concatenate((y0_species,y0_resources),axis=None)
time = np.linspace(0,1000,100)


def surviving_species(model):
    """Calculates number of surviving species"""
    s = model.time_course_assim(y0)
    res = s.simulate(15000)
    res_species = res[1][-1,:100]
    return sum(res_species > 1), res
     
count = 0    
meta_dict2 = {}
meta_species_time_dict = {}
for k,v in meta_dict.items():
    print(k)
    model_dict2 = {}
    species_time_dict = {}
    for l,w in v.items():
        print(l)
        count += 1
        print(count)
        model_dict2[l], species_time_dict[l] = surviving_species(w)
        print(model_dict2[l])
    meta_dict2[k] = model_dict2
    meta_species_time_dict[k] = species_time_dict
        
#%%
pickle.dump(meta_dict2, open("meta_dict2.p", "wb" ) )
pickle.dump(meta_species_time_dict, open("meta_species_time_dict.p", "wb" ) )

#%%

mean_pDF = pd.DataFrame(meta_dict2).mean()

names = [eval(x) for x in mean_pDF.index]
names = [np.append(np.array(names[i]),mean_pDF.values[i]) for i in range(len(names))]

blah = pd.DataFrame(names)
blah.columns = ['a','b','c']
blah = blah.pivot(index='a',columns='b',values='c')
blah = blah.T.sort_index(ascending=False)


fig,ax = plt.subplots(figsize=(15,12))
sns.heatmap(blah, cmap="bwr")
plt.xlabel('l',size=30)
plt.ylabel('$w_0\kappa_0$',size=25)
plt.xticks(fontsize=30)
plt.yticks(fontsize=27,rotation=360)
cax = plt.gcf().axes[-1]
cax.tick_params(labelsize=30)
plt.savefig("Reproduce_MA.png")
plt.show()

#%%

def rank_abundance(meta_dict,name):
    """Calculates the rank abundance plots"""
    colors = 1
    plt.figure(figsize=(17,13))
    for models, res in meta_dict.items():
        surviving_species = res[1][-1,:100]
        surviving_species = surviving_species[surviving_species>1]
        norm_surviving_species = surviving_species/surviving_species.sum()
        norm_surviving_species[::-1].sort()
        cmap = plt.get_cmap('Blues')
        c = cmap(colors)
        colors += 30
        plt.plot(range(len(norm_surviving_species)),norm_surviving_species,'o-', c=c)
    plt.yscale('log')
    plt.xlim(0,45)
    plt.ylim(0.001,1)
    plt.xlabel('Rank',size=40)
    plt.ylabel('Relative Abundance',size=40)
    plt.tick_params(labelsize=40)
    plt.savefig(name)
    
    
    
a = rank_abundance(meta_species_time_dict['(0.2, 330)'],'RA_02_330.png')
b = rank_abundance(meta_species_time_dict['(0.9, 1000)'], 'RA_09_1000.png')
c = rank_abundance(meta_species_time_dict['(0.6, 90)'], 'RA_06_90.png')


