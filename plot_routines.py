#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plotting routines
"""


import itertools
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.patches import Circle, Arrow



__author__ = 'Tim Nies'
__credit__='Tim Nies'
__liscence__=''
__vesion__='0.1'
__maintainer__='Tim Nies'



def Plot_InvasionFitness_NumberSurvivingSpecies(meta_steadystates,meta_F_log,number_species,figsize=(15,12), save=False,
                                           filename="InvasionFitness_NumberSurvivingSpecies.png",show=True):
    
    """Creates a plot that shows the relationship between the Invasion fitness
    and the number of surviving species (inavder inclusive) after invasion
    """
    meta_steadystates = np.array(meta_steadystates)[:,:number_species]  
    idx_of_surviving_species = [np.where(i>1)[0] for i in meta_steadystates]  
    number_of_surviving_species = np.array([len(i) for i in idx_of_surviving_species])
    converged_F_logs = np.round(np.array(meta_F_log)[:,-1,-1],4)
    ax,fig = plt.subplots(figsize=figsize)
    plt.scatter(converged_F_logs,number_of_surviving_species)
    plt.xlabel("Invasion fitness",size=20)
    plt.ylabel("Number of surviving Species",size=20)
    plt.xticks(fontsize=20,rotation=90)
    plt.yticks(fontsize=20)
    if save:
        plt.savefig(filename, format="png")
    if show:
        plt.show()



#def Plot_population_reactions(pop_reaction,meta_steadystates,meta_F_log,ss,number_species,
#                              figsize=(15,12),save=False,filenames='Pop_reaction',show=True):
#    """Calculates the fractional increase and decrease, regarding the species 
#    abundance in the original stable community
#    """
#    test = np.array(pop_reaction)
#    meta_steadystates = np.array(meta_steadystates)[:,:number_species]  
#    idx_of_surviving_species = [np.where(i>1)[0] for i in meta_steadystates]  
#    number_of_surviving_species = np.array([len(i) for i in idx_of_surviving_species])
#    converged_F_logs = np.round(np.array(meta_F_log)[:,-1,-1],4)
#    for i in range(number_species-1):
#        if abs(sum(test[:,i])) > 0:
#            plt.figure(figsize=figsize)
#            plt.scatter(converged_F_logs,test[:,i]/ss[i]*100, c = number_of_surviving_species,cmap=plt.cm.get_cmap('jet',
#                        max(number_of_surviving_species)), label = ('species %d' % i))
#            plt.xlabel("Invasion fitness",fontsize=20)
#            plt.ylabel("Abundance Change [%]",fontsize=20)
#            plt.title(('steady state %d') % ss[i])
#            plt.axhline(0,0,linestyle='--',c='r')
#            plt.xticks(fontsize=20,rotation=90)
#            plt.yticks(fontsize=20)
#            plt.legend()
#            plt.colorbar()
#            if save:
#                plt.savefig(filenames + str(i) + ".png")
#        if show:
#            plt.show()
            
def Plot_population_reactions(pop_reaction,meta_steadystates,meta_F_log,ss,number_species,
                              figsize=(15,12),save=False,filenames='Pop_reaction',show=True):
    """Calculates the fractional increase and decrease, regarding the species 
    abundance in the original stable community
    """
    test = np.array(pop_reaction)
    meta_steadystates = np.array(meta_steadystates)[:,:number_species]  
    idx_of_surviving_species = [np.where(i>1)[0] for i in meta_steadystates]  
    number_of_surviving_species = np.array([len(i) for i in idx_of_surviving_species])
    converged_F_logs = np.round(np.array(meta_F_log)[:,-1,-1],4)
    cmap = plt.cm.jet
    for i in range(number_species-1):
        if abs(sum(test[:,i])) > 0:
            norm = mpl.colors.BoundaryNorm(np.arange(min(number_of_surviving_species),max(number_of_surviving_species)+2,1), cmap.N)
            plt.figure(figsize=figsize)
            plt.scatter(converged_F_logs,test[:,i]/ss[i]*100, c = number_of_surviving_species,cmap=cmap, norm=norm, label = ('species %d' % i))
            plt.xlabel("Invasion fitness",fontsize=20)
            plt.ylabel("Abundance Change [%]",fontsize=20)
            #plt.title(('steady state %d') % ss[i])
            plt.axhline(0,0,linestyle='--',c='r')
            plt.xticks(fontsize=20,rotation=90)
            plt.yticks(fontsize=20)
            #plt.legend()
            cbar = plt.colorbar()
            cbar.set_label('Number of surviving species', rotation=90, fontsize=20)
            cbar.ax.tick_params(labelsize=20)
            labels = np.arange(min(number_of_surviving_species),max(number_of_surviving_species)+2,1)
            loc    = labels + .5
            cbar.set_ticks(loc)
            cbar.set_ticklabels(labels)
            if save:
                plt.savefig(filenames + str(i) + ".png")
        if show:
            plt.show()



def Plot_GA1(meta_F_log,figsize=(15,12),save=False,filenames='metaF_Generations.png',show=True):
    """Plots the convergent behaviour of invasion fitness over Generations
    """
    plt.figure(figsize=(15,12))
    plt.plot(np.mean(meta_F_log,axis=2).T)
    plt.xlabel("Generations",fontsize=20)
    plt.ylabel("average invasion fitness",fontsize=20)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    if save:
        plt.savefig(filenames)
    if show:
        plt.show()
        


def Plot_C(M,figsize=(15,12),save=False,filename='ArrowPlot_C.png',show=True,legend=False):
    """Only for 2 resources models"""
    fig, ax = plt.subplots(figsize=figsize)
    plt.xlim(0,1)
    plt.ylim(0,1)
    circle = Circle((0, 0), 1, facecolor='none', edgecolor='limegreen', linewidth=3, alpha=0.5)
    ax.add_patch(circle)
    plt.ylabel('Resource 2',fontsize=25)
    plt.xlabel('Resource 1',fontsize=25)
    plt.tick_params(labelsize=25)
    for i in range(np.shape(M)[0]):
        c = M[i,:]
        arrow = Arrow(0,0,c[0],c[1],width=0.1, color=random_colors(), label = ("species %s" % str(i+1)))
        ax.add_patch(arrow)
    if save:
        plt.savefig(filename)
    if legend:
        plt.legend()
    if show:
        plt.show()



def random_colors():
    color = np.random.choice(np.linspace(0,0.9,10),size=3)
    return tuple(color)



def Plot_SequentialInvasion1(res,time,species_count,save=False,filename='SequentialInvasion.png',show=True,xlim=False):
    species = np.array([res[i][:j] for i,j in enumerate(species_count)])
    res2 = np.array(list(itertools.zip_longest(*species,fillvalue=0))).T
    fig,ax = plt.subplots(figsize=(15,12))
    ax.plot(time,res2,linewidth=4)
    ax.ticklabel_format(useOffset=False)
    plt.xlabel('Time [a.u]',size=25)
    plt.ylabel(r'Consumer Pop. $N_i$',size=25)
    plt.tick_params(labelsize=25)
    if xlim:
        plt.xlim(0,xlim)
    if save:
        plt.savefig(filename)
    if show:
        plt.show()
   
    
    
def Plot_SequentialInvasion2(res,time,species_count,save=False,filename='SequentialInvasion_MassSum.png',show=True,xlim=False):
    species = np.array([res[i][:j] for i,j in enumerate(species_count)])
    res2 = np.array(list(itertools.zip_longest(*species,fillvalue=0))).T
    fig,ax = plt.subplots(figsize=(20,15))
    ax.plot(time,np.sum(res2,axis=1))
    ax.ticklabel_format(useOffset=False)
    plt.ticklabel_format(style='sci', axis='y')
    plt.xlabel('Time [a.u]',size=30)
    plt.ylabel(r'Overall abundance $\sum_{i}N_i$',size=25.8)
    plt.tick_params(labelsize=30)
    if xlim:
        plt.xlim(0,xlim)
    if save:
        plt.savefig(filename)
    if show:
        plt.show()
   

       
#%%
#import os 
#import pickle   
#data_dir = '/home/tim/Desktop/Bacgrowth/Git/ecomodels/Models/data/Experiment_Invasion'
#dirs = os.listdir(data_dir)
#Flog = 'Invasion_1000points_40s30r_sameD_metaFlog.p'
#meta_ss = 'Invasion_1000points_40s30r_sameD_metasteadystates.p'
#ss = 'Invasion_1000points_40s30r_sameD_org_ss.p'
#pop_reaction = 'Invasion_1000points_40s30r_sameD_pop_reaction.p'
#
#Flog = pickle.load( open( data_dir + '/Invasion_9/' + Flog, 'rb' ))
#meta_ss = pickle.load( open( data_dir + '/Invasion_9/' + meta_ss, 'rb' ))
#ss = pickle.load( open( data_dir + '/Invasion_9/' + ss, 'rb' ))
#pop_reaction = pickle.load( open( data_dir + '/Invasion_9/' + pop_reaction, 'rb' ))
###
###%%
#
#Plot_InvasionFitness_NumberSurvivingSpecies(meta_ss,Flog,41,(10,8), save=True)
#Plot_population_reactions(pop_reaction,meta_ss,Flog,ss,41,(10,8), save=True)
##Plot_GA1(Flog,save=True)

#%%

#data_dir = '/home/tim/Desktop/Bacgrowth/Texte/VariantB'
#dirs = os.listdir(data_dir)
#t = pickle.load( open(data_dir + '/t_single_seqInv.p', 'rb'))
#seqC = pickle.load( open(data_dir + '/seqC_single_seqInv.p', 'rb'))
#res = pickle.load( open(data_dir + '/res_single_seqInv.p', 'rb'))
#species_count = pickle.load( open(data_dir + '/species_count_single_seqInv.p', 'rb'))       
#        
#
#Plot_SequentialInvasion2(res,t,species_count,save=True)
#%%    
__all__ = ['Plot_InvasionFitness_NumberSurvivingSpecies','Plot_population_reactions',
           'Plot_GA1','Plot_C','Plot_SequentialInvasion1','Plot_SequentialInvasion2']