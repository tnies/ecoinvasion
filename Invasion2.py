#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Invasion analysis with constraint preference sum. For normalization:
    -- normalize getConsumerMatrix
    --
"""

import pickle
import numpy as np
import matplotlib.pyplot as plt
from models import Mcrm
from utils import getMetabolismMatrix, getConsumerMatrix, get_invader_model, save_model
from GeneticAlg import GA_analysis


__author__ = 'Tim Nies'
__credit__='Tim Nies'
__liscence__='GPLv3'
__vesion__='0.1'
__maintainer__='Tim Nies'




model = Mcrm()
l_invasion = 0.8
n_resources = 30
n_species = 40

#%% Basic model

#set metaparams
model.change_metaparams({'number_species':n_species,'number_resources':n_resources,
                        'h':'renewable', 'sigma': 'type I'})

number_species = model.metaparams['number_species']
number_resources = model.metaparams['number_resources']

#set parameters
model.params["C"] = getConsumerMatrix(model,normalize=True,specialists=False)
model.params["D"] = getMetabolismMatrix(model)
model.params['k'] = np.zeros(number_resources)
model.params['k'][0] = 10e4


#%% This line is for reoeating an experiment but with a different consumer
#matrix does only work in IDE

#model.params["C"] = getConsumerMatrix(model,normalize=True,specialists=False)



#%%
model.params['l'] = np.repeat(l_invasion,number_resources)

#initial values
y0_resources = np.zeros(number_resources)
y0_resources[0] = 1e2
y0_species = np.repeat(10e3,number_species)
y0 = np.concatenate((y0_species,y0_resources),axis=None)
time = np.linspace(0,10000,10000) 

#simulate
res = model.time_course(time,y0)
res = np.array(res)



#visualize
fig = plt.figure(figsize=(15,8))
ax = plt.subplot(111)
for i in range(number_species):
    ax.plot(time,res[:,i], label = "species %s" % str(i+1))
ax.tick_params(labelsize=20)
plt.ylim(10e-10,10e5)
plt.xscale('log')
plt.xlabel('Time [a.u.]', fontsize=20)
plt.yscale('log')
plt.ylabel('Consumer.Pop $N_i$', fontsize=20)
#plt.savefig("l_0_8_Consumers100_Resources_30.png", format="png")
#plt.show()


#steadystate and test if values stay constant
fig = plt.figure(figsize=(15,8))
ss = model.steadystate(y0,toleranz=1e-12)
res = model.time_course(time,ss)
res = np.array(res)
plt.plot(time,res)
#plt.show()

#save steady state
save_model(model,"model.p")
pickle.dump(ss, open("Invasion_1000points_40s30r_sameD_org_ss.p", "wb" ) )


#%% INVASION: SLOPE OF INVADER MUST BE POSITIVE


model2 = get_invader_model(model,normalize=True)
number_species = model2.metaparams['number_species']
number_resources = model2.metaparams['number_resources']


invasion_start = 1 #for invasion fitness must be 1 otherwise divide invasion fitness by invasion_start
y0 = np.concatenate((ss[:number_species-1],invasion_start,ss[number_species-1:]),axis=None)
time = np.linspace(0,10000,10000) 
res = model2.time_course(time,y0)
res = np.array(res)


ax = plt.subplot(111)
for i in range(number_species):
    if sum(res[:,i]) > 10e-5:
        plt.plot(time[:1000],res[:1000,i],label = ('species %d' % i), linewidth=8)
ax.tick_params(labelsize=40)
plt.xlabel('Time', fontsize=40)
plt.ylabel('Consumer.Pop $N_i$', fontsize=40)
#plt.show()

save_model(model2,"model_invasion.p")


#%% Genetic Alg


pop_reaction, meta_steadystates, meta_org_C, meta_matrices,meta_fitness_log, meta_F_log = GA_analysis(1000,model2,y0,model.params['C'],threshold=1)
pickle.dump(meta_matrices, open("Invasion_1000points_40s30r_sameD_metamatrices.p", "wb" ) )
pickle.dump(meta_F_log, open("Invasion_1000points_40s30r_sameD_metaFlog.p", "wb" ) )
pickle.dump(meta_steadystates, open("Invasion_1000points_40s30r_sameD_metasteadystates.p", "wb" ) )
pickle.dump(meta_fitness_log, open("Invasion_1000points_40s30r_sameD_metafitness.p", "wb" ) )
pickle.dump(meta_org_C, open("Invasion_1000points_40s30r_sameD_meta_org_c.p", "wb" ) )
pickle.dump(pop_reaction, open("Invasion_1000points_40s30r_sameD_pop_reaction.p", "wb" ) )
pickle.dump(model2.params["D"], open("D_matrix.p", "wb" ) )

#%%
meta_steadystates = np.array(meta_steadystates)[:,:number_species]  
idx_of_surviving_species = [np.where(i>1)[0] for i in meta_steadystates]  
number_of_surviving_species = np.array([len(i) for i in idx_of_surviving_species])
converged_F_logs = np.round(np.array(meta_F_log)[:,-1,-1],4)
plt.figure(figsize=(15,12))
plt.scatter(converged_F_logs,number_of_surviving_species)
plt.xlabel("Invasion fitness")
plt.ylabel("Number of surviving Species")
plt.xticks(fontsize=40)
plt.yticks(fontsize=40)
plt.savefig("1000points_40s30r_sameD_InvasionFitness_vs_NumberSpecies.png", format="png")
#plt.show()
