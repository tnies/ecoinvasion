#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Contains several useful utilities for the analysis of the model.
Conatins some functions for the genetic algorithms
"""

import pickle
import numpy as np
from models import Mcrm
from itertools import product
from scipy.integrate import ode



__author__ = 'Tim Nies'
__credit__='Tim Nies'
__liscence__='GLPv3'
__vesion__='0.1'
__maintainer__='Tim Nies'



def getConsumerMatrix(model, normalize=False, specialists=False, threshold = 1):
    """get random normalized preference matrix
    """
    number_species = model.metaparams['number_species']
    number_resources = model.metaparams['number_resources']
    C = np.random.rand(number_species,number_resources)
    if normalize == True:
        C = (C.T/np.sum(C,axis=1)*threshold).T
    if specialists:
        for i in range(np.random.choice(number_species)):
            idx = np.random.choice(range(number_species))
            idx_res = np.random.choice(range(number_resources))
            tmp = np.zeros(number_resources)
            tmp[idx_res] = threshold
            C[idx,:] = tmp
    return C
    


def getMetabolismMatrix(model,sort='rand',single=False):
    """should not be used anymore
    """
    D = np.random.rand(model.metaparams['number_resources'],
                       model.metaparams['number_resources'])
    D_norm = D/np.sum(D,axis=0)
    if single:
        return D_norm
    else:
        D_mult = []
        for i in range(model.metaparams['number_species']):
            D_mult.append(D_norm)
        return D_mult



def get_Average_Preferences(C):
    """
    Calculates the average preference for resources of a consumer
    Input:  c_ia matrix
    Output: x_i array with average preferences
    """
    return np.mean(C,axis=1)



def get_Overall_Production(D):
    """
    Calculates the overall production of a resource
    Input: D matrix (protometabolims)
    """
    return np.sum(D,axis=1)



def get_angle(a,b):
    """Get angle between two vectors in rad. Use np.dgrees to convert"""
    c = np.dot(a,b)/(np.linalg.norm(a,ord=2)*np.linalg.norm(b,ord=2))
    angle = np.arccos(c)
    return angle


def get_invader_model(model,normalize=False,threshold=1):
    """Creates the invader model with one additional species"""
    model2 = Mcrm()
    meta_params = model.metaparams.copy()
    meta_params['number_species'] += 1
    model2.change_metaparams(meta_params)
    #add D matrix really weird
    D = list(model.params['D'].copy())
    D.append(D[-1])
    model2.params['D'] = np.array(D)
    #add invader prefernces to C matrix
    c_inv = np.random.rand(model.metaparams['number_resources'])
    if normalize:
        c_inv = c_inv/sum(c_inv)*threshold
    model2.params['C'] = np.vstack((model.params['C'],c_inv))
    #change l and k accordingly:
    model2.params['l'] = model.params['l'].copy()
    model2.params['k'] = model.params['k'].copy()
    return model2



def get_binary_invader(number_resources,number_preferences):
    x = np.zeros(number_resources)
    idx = np.random.choice(range(number_resources),number_preferences,replace=False)
    x[idx] = 1
    return x



def save_model(model,name):
    """Saves the model meta- and parameters"""
    pickle.dump((model.params,model.metaparams),open(name,"wb"))



def get_interaction_matrix(C): #ToDO check formula in paper. Do not understand normalisation.
    """calculates unnormalized niche overlaps"""
    return np.dot(C,C.T)



def get_number_of_surviving_species(res,number_species):
    """Calculates the number of surviving species after invasion"""
    return np.sum(np.array(res)[:,:number_species] > 10e-5,axis=1)



def extend_initial_state(y0,number_species,invasion_start):
    """inserts an additional species"""
    y0_new = np.concatenate((y0[:number_species],
                             invasion_start,y0[number_species:]),axis=None)
    return y0_new



def get_binary_C_matrix(number_species,number_resources,number_preferences):
    """Creates a binary C matrix"""
    C = np.zeros(number_resources)
    idx = np.random.choice(range(number_resources),number_preferences,replace=False)
    C[idx]=1
    return np.array([np.random.permutation(C) for _ in range(number_species)])


     
def get_invader_probability(C,c_inv): #ToDO How is this defined. Is this useful?
    n_resources, n_species = np.shape(C)
    niches = 1/(np.sum(C,axis=0)+1)
    probs = np.repeat(1/n_resources,n_species)*niches
    norm_probs = probs/sum(probs)
    return np.prod((norm_probs*c_inv)[c_inv !=0])



def Nc_metric(C):
    """Calculates the Nc metric"""
    marginal = np.sum(C,axis=0)
    Nc_col = (marginal*(marginal-1))/2
    return np.sum(Nc_col)



def get_overlap(pair,norm_to_first_species=True):
    """Cacluclates the overlapping resources and normalize to number of resources
    available to invader -> only valid when binary consumer matrix"""
    overlap = len(np.where(np.sum(pair == 1,axis=1)==2)[0])
    if norm_to_first_species:
        return overlap/sum(pair[:,0])
    else:
        overlap
    return overlap



def get_overlap_matrix(C):
    m,n = np.shape(C)
    idx_pairs = list(product(range(m),range(m)))
    Overlap_matrix = np.array([get_overlap(C.T[:,idx_pairs[i]],norm_to_first_species=False) for i in range(len(idx_pairs))]).reshape(m,m)
    return Overlap_matrix
    

#DEPRECATED
def old_invasion_time_course(model,t,y0,invader_preferences,number_species,inv_p=0.001):
    """makes a simulation of a sequentially invaded population. each time step
    the invasion happens with constant probability 0.001. Only a valid method
    when used together with equally spaced time intervals
    """
    Integrator = ode(model.system).set_integrator('lsoda').set_initial_value(y0,0)
    cnt = 1
    res = [y0]
    species_count = [number_species]
    while cnt < len(t) and Integrator.successful():
        if np.random.rand() < inv_p:
            print(cnt)
            #change initial values and metaparams
            y0 = extend_initial_state(res[-1],number_species,1)
            idx = np.random.choice(range(len(invader_preferences)))
            
            #invasion_model
            model = get_invader_model(model)
            model.params['C'][-1,:] = invader_preferences[idx]
                
            #new integrator
            Integrator = ode(model.system).set_integrator('lsoda').set_initial_value(y0,t[cnt-1])
            number_species += 1
        species_count.append(number_species)
        res.append(Integrator.integrate(t[cnt]))
        cnt += 1
    return res, species_count


def invasion_time_course(model,t,y0,invader_preferences,number_species,average_time=10,invasion_start=10,threshold=False):
    """makes a simulation of a sequentially invaded population. The time to 
    the next event is drawn from a exponential distribution
    """
    yl = []
    Integrator = ode(model.system).set_integrator('lsoda').set_initial_value(y0,0)
    res = [y0]
    time = np.array([0])
    species_count = [number_species]
    dts = []
    while time[-1] < t:
        print(time[-1])
        
        #interval until invasion
        dt = np.random.exponential(average_time) #time to next invasion event
        dts.append(time[-1]+dt)
        interval = np.linspace(time[-1],time[-1]+dt,int(np.ceil(dt))*10)
        time = np.concatenate((time,interval[1:]),axis=None)
        cnt = 1
        while cnt < len(interval-1) and Integrator.successful():
            if threshold:
                tmp = Integrator.integrate(interval[cnt])
                tmp[tmp < threshold] = 0
                res.append(tmp)
            else:
                res.append(Integrator.integrate(interval[cnt]))
            cnt += 1
            species_count.append(number_species)
        
        #invasion
        yl.append(y0)
        y0 = extend_initial_state(res[-1],number_species,invasion_start)
        idx = np.random.choice(range(len(invader_preferences)))
         
        #invasion_model
        model = get_invader_model(model)
        model.params['C'][-1,:] = invader_preferences[idx]
        
        #new integrator
        Integrator = ode(model.system).set_integrator('lsoda').set_initial_value(y0,time[-1])
        number_species += 1
        
    return res, time, species_count, dts, model.params['C'], yl

        
def ShannonEntropy(C):
    """Calculate the Shannon entropy
    """
    return np.sum(-C*np.log2(C),axis=1)           
            

###############################################################################
#   FROM COMMUNITYSIMULATOR BY MARSLAND III ET AL. 2019 PLOS Comp. Biol.
###############################################################################

# def Simpson(N):
#     p = N/np.sum(N)
#     return 1./np.sum(p**2)


# def Shannon(N):
#     p = N/np.sum(N)
#     p = p[p>0]
#     return np.exp(-np.sum(p*np.log(p)))


# def BergerParker(N):
#     p = N/np.sum(N)
#     return 1./np.max(p)


# def Richness(N,thresh=0):
#     return np.sum(N>thresh)



###############################################################################
###############################################################################
    


__all__ = ['getConsumerMatrix','getMetabolismMatrix',
           'get_Average_Preferences','get_Overall_Production','get_angle',
           'get_interaction_matrix','get_number_of_surviving_species',
           'get_invader_probability','get_invader_model','get_overlap',
           'get_binary_C_matrix','get_overlap_matrix','extend_initial_state',
           'invasion_time_course','ShannonEntropy']


