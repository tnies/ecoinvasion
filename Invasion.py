#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Invasion experiments. How does a steady state ecological system changes when 
a species that is different to the resident species tries to invade the 
system.

THIS FILE IS DEPRICATED AND SHOULD NOT BE USED. 
"""
import pickle
import itertools
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from models import Mcrm
import pandas as pd
from utils import get_angle, get_interaction_matrix, getConsumerMatrix
from utils import getMetabolismMatrix,get_invader_model, save_model, invasion_time_course
from GeneticAlg import GA_analysis



__author__ = 'Tim Nies'
__credit__='Tim Nies'
__liscence__='GPLv3'
__vesion__='0.1'
__maintainer__='Tim Nies'



model = Mcrm()
l_invasion = 0.8
n_resources = 30
n_species = 40


#%% Basic model

#set metaparams
model.change_metaparams({'number_species':n_species,'number_resources':n_resources,
                         'h':'renewable', 'sigma': 'type I'})

number_species = model.metaparams['number_species']
number_resources = model.metaparams['number_resources']

#set parameters
model.params["C"] = getConsumerMatrix(model)
model.params["D"] = getMetabolismMatrix(model)
model.params['k'] = np.zeros(number_resources)
model.params['k'][0] = 10e4
model.params['l'] = np.repeat(l_invasion,number_resources)

#initial values
y0_resources = np.zeros(number_resources)
y0_resources[0] = 1e2
y0_species = np.repeat(10e3,number_species)
y0 = np.concatenate((y0_species,y0_resources),axis=None)
time = np.linspace(0,10000,10000) 

#simulate
res = model.time_course(time,y0)
res = np.array(res)



#visualize
#fig = plt.figure(figsize=(20,12))
ax = plt.subplot(111)
for i in range(number_species):
    ax.plot(time,res[:,i], label = "species %s" % str(i+1))
ax.tick_params(labelsize=20)
plt.ylim(10e-10,10e5)
plt.xscale('log')
plt.xlabel('Time', fontsize=20)
plt.yscale('log')
plt.ylabel('Consumer.Pop $N_i$', fontsize=20)
#plt.savefig("high_leakage_l_0_8_Consumers.svg", format="svg")
#plt.show()


#steadystate and test if values stay constant
ss = model.steadystate(y0,toleranz=1e-12)
res = model.time_course(time,ss)
res = np.array(res)
plt.plot(time,res)
#plt.show()

#save steady state
save_model(model,"model.p")
pickle.dump(ss, open("Invasion_1000points_40s30r_sameD_org_ss.p", "wb" ) )

#%% INVASION: SLOPE OF INVADER MUST BE POSITIVE


model2 = get_invader_model(model)
number_species = model2.metaparams['number_species']
number_resources = model2.metaparams['number_resources']

#for checking invade
#c_inv = meta_matrices[34]
#C2 = np.vstack((C,c_inv))
#model2.params['C'] = C2


invasion_start = 1
y0 = np.concatenate((ss[:number_species-1],invasion_start,ss[number_species-1:]),axis=None)
time = np.linspace(0,10000,10000) 
res = model2.time_course(time,y0)
res = np.array(res)

#fig = plt.figure(figsize=(20,12))
ax = plt.subplot(111)
for i in range(number_species):
    if sum(res[:,i]) > 10e-5:
        plt.plot(time[:1000],res[:1000,i],label = ('species %d' % i), linewidth=8)
#plt.yscale('log')
#plt.ylim(10e0,10e4)
#plt.legend()
ax.tick_params(labelsize=40)
plt.xlabel('Time', fontsize=40)
plt.ylabel('Consumer.Pop $N_i$', fontsize=40)
#plt.show()

#converged_F_logs[411]
#plt.savefig("invasion_fitness_0.2298.svg", format="svg")
#model2.system(0,y0)[number_species-1]

#save invasion model
save_model(model2,"model_invasion.p")
#%%
###############################################################################
#                           Genetic Algorithm Analysis                               #
###############################################################################

pop_reaction, meta_steadystates, meta_org_C, meta_matrices,meta_fitness_log, meta_F_log = GA_analysis(1000,model2,y0,model.params['C'])
pickle.dump(meta_matrices, open("Invasion_1000points_40s30r_sameD_metamatrices.p", "wb" ) )
pickle.dump(meta_F_log, open("Invasion_1000points_40s30r_sameD_metaFlog.p", "wb" ) )
pickle.dump(meta_steadystates, open("Invasion_1000points_40s30r_sameD_metasteadystates.p", "wb" ) )
pickle.dump(meta_fitness_log, open("Invasion_1000points_40s30r_sameD_metafitness.p", "wb" ) )
pickle.dump(meta_org_C, open("Invasion_1000points_40s30r_sameD_meta_org_c.p", "wb" ) )
pickle.dump(pop_reaction, open("Invasion_1000points_40s30r_sameD_pop_reaction.p", "wb" ) )
pickle.dump(model2.params["D"], open("D_matrix.p", "wb" ) )


#%%%
#meta_matrices = pickle.load( open( "data/060819/Invasion_1000points_40s30r_sameD_metamatrices.p", "rb" ) )
#meta_F_log = pickle.load(open("data/060819/Invasion_1000points_40s30r_sameD_metaFlog.p","rb"))
#meta_steadystates = pickle.load(open("data/060819/Invasion_1000points_40s30r_sameD_metasteadystates.p","rb"))


#%%##################################################################################################
#########################   ANALYSIS  ###############################################################
#####################################################################################################

plt.figure(figsize=(15,12))
plt.plot(np.mean(meta_F_log,axis=2).T)
plt.xlabel("Generations",fontsize=20)
plt.ylabel("average invasion fitness",fontsize=20)
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
plt.savefig("1000points_40s30r_sameD_metFlog.png")
#plt.show()

blah = np.array(meta_matrices).reshape(1000,n_resources)

for i in range(5):
    plt.scatter(blah[:,i],blah[:,i-1],alpha=0.01,color='g')
#plt.show()
    
sns.kdeplot(np.round(np.array(meta_F_log)[:,-1,-1],4))
plt.xlim(0,0.62)
plt.savefig("1000points_40s30r_sameD_invasion_fitness.png")

#%% number of survivers as function of invasion fitness

meta_steadystates = np.array(meta_steadystates)[:,:number_species]  
idx_of_surviving_species = [np.where(i>1)[0] for i in meta_steadystates]  
number_of_surviving_species = np.array([len(i) for i in idx_of_surviving_species])
converged_F_logs = np.round(np.array(meta_F_log)[:,-1,-1],4)

plt.figure(figsize=(15,12))
plt.scatter(converged_F_logs,number_of_surviving_species)
plt.xlabel("Invasion fitness",fontsize=40)
plt.ylabel("Number of surviving Species",fontsize=40)
plt.xticks(fontsize=40)
plt.yticks(fontsize=40)
plt.savefig("1000points_40s30r_sameD_InvasionFitness_vs_NumberSpecies.svg", format="svg")
plt.show()

#%% Reactions of established species on invader as function of invasion fitness

test = np.array(pop_reaction)
 
for i in range(number_species-1):
    if abs(sum(test[:,i])) > 0:
        plt.figure(figsize=(15,12))
        plt.scatter(converged_F_logs,test[:,i]/ss[i]*100, c = number_of_surviving_species,cmap=plt.cm.get_cmap('jet', max(number_of_surviving_species)), label = ('species %d' % i))
        plt.xlabel("Invasion fitness",fontsize=20)
        plt.ylabel("Abundance Change [%]",fontsize=20)
        plt.title(('steady state %d') % ss[i])
        plt.axhline(0,0,linestyle='--',c='r')
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        plt.legend()
        plt.colorbar()
        plt.savefig("1000points_40s30r_sameD_AbundanceChange" + str(i) + ".png")
        #plt.show()

#%% compare angles

angles = []
for i in meta_org_C:
    a = [np.degrees(get_angle(i[j,:],i[-1,:])) for j in range(np.shape(i)[0]-1)]
    angles.append(a)

#angles of surviving species   
surviving_angles = [np.array(angles[i])[idx_of_surviving_species[i][:-1]] for i in range(len(angles))]

#angles of extinct species
extinct_angles = []
for i in range(len(angles)):
    mask=np.full(40,True,dtype=bool)
    mask[idx_of_surviving_species[i][:-1]] = False
    extinct_angles.append(np.array(angles)[i][mask])
    
angles_DF = pd.DataFrame(angles)
angles_DF.plot.box()


#%% compare lengths
lengths = []
for i in meta_org_C:
    lengths.append(np.linalg.norm(i,ord=2,axis=1))
    
surviving_lengths = [lengths[i][idx_of_surviving_species[i]] for i in range(len(lengths))]
surviving_lengths_means = [np.mean(i[:-1]-i[-1])/i[-1]*100 for i in surviving_lengths]

fig, ax = plt.subplots()
plt.figure(figsize=(15,12))
plt.scatter(converged_F_logs,surviving_lengths_means, c=number_of_surviving_species,cmap=plt.cm.get_cmap('jet', max(number_of_surviving_species)))
plt.colorbar()
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
plt.xlabel("Invasion fitness",fontsize=20)
plt.ylabel("Mean Surviving Species Vector Lengths Difference",fontsize=20)
plt.savefig("1000points_40s30r_sameD_Mean_Surviving_Species_Vector_Lengths_Difference.svg", format="svg")

#%%

mean_meta_F_log = np.mean(meta_F_log,axis=2)
bin_min, bin_max = mean_meta_F_log.min(), mean_meta_F_log.max()
bins = np.linspace(bin_min,bin_max,300)
dist = np.zeros((300-1,1000))
for t, meanF in enumerate(mean_meta_F_log):
    dist[:,t],_= np.histogram(meanF,bins=bins)
dmask = dist
dmask[dmask==0.0] = np.nan
plt.figure(figsize=(12,15))
plt.imshow(dmask,aspect='auto',extent=[0,1000,bin_min,bin_max],cmap='viridis',origin='bottom')
plt.xlabel('Generation', fontsize=20)
plt.ylabel('Invasion Fitness',fontsize=20)
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
plt.savefig("1000points_40s30r_sameD_Mean_density_invasionFitness_per_Generation.png")
    
#%%
idx = np.where(ss[:number_species-1] != 0)
interaction = np.array([np.argmax(get_interaction_matrix(i)[-1][idx]) for i in meta_org_C])

fig, ax = plt.subplots()
sns.countplot(interaction)
#plt.show()

fig, ax = plt.subplots()
sns.countplot(number_of_surviving_species)
#plt.show()


#%%

res, time2, species_count, dts = invasion_time_course(model,3000,ss,np.array(meta_org_C)[:,-1,:],40,15)
species = np.array([res[i][:j] for i,j in enumerate(species_count)])
res2 = np.array(list(itertools.zip_longest(*species,fillvalue=0))).T
#%%
fig,ax = plt.subplots()
ax.plot(time2,res2,linewidth=4)
ax.ticklabel_format(useOffset=False)
plt.xlim(0,500)
#plt.show()

fig,ax = plt.subplots()
ax.plot(time2,np.sum(res2,axis=1))
#ax.vlines(dts[7],min(np.sum(res2,axis=1)),max(np.sum(res2,axis=1)))
ax.ticklabel_format(useOffset=False)
#plt.show()

#
#


