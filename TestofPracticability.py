#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Tests if approximation methods are able to create realistic microbial populations
"""

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from models import Mcrm


__author__ = 'Tim Nies'
__credit__='Tim Nies'
__liscence__='GPLv3'
__vesion__='0.1'
__maintainer__='Tim Nies'


#%% E.coli subtype model in aerobe conditions

model = Mcrm()
n_resources = 5
n_species = 5
org_C = np.array([0.37,0.3,0.08,0.09,0.16])

#set metaparams
model.change_metaparams({'number_species':n_species,'number_resources':n_resources,
                        'h':'renewable', 'sigma': 'type I'})

number_species = model.metaparams['number_species']
number_resources = model.metaparams['number_resources']

#set parameters
model.params['l'] = np.array([0.321,0.329,0.545,0.471,0.4])
model.params['D'] = np.array([[0,0,0,0,0],[0,0,0,0,0],[0.35,0.8,0,0.58,0.46],[0.4,0.12,0.6,0,0.54],[0.25,0.081,0.4,0.42,0]])
model.params['D'] = np.array([model.params['D'] for i in range(5)])
model.params['C'] = np.array([[0.39,0.27,0.036,0.064,0.24],[0.25,0.23,0.099,0.17,0.25],[0.43,0.25,0.071,0.084,0.17],[0.22,0.39,0.12,0.078,0.19],[0.4,0.29,0.084,0.089,0.14]])
model.params['k'] = np.zeros(number_resources)
model.params['k'][0] = 10e4
model.params['k'][1] = 10e4

#initial values
y0_resources = np.zeros(number_resources)
y0_resources[0] = 1e2
y0_resources[1] = 1e2
y0_species = np.repeat(10e3,number_species)
y0 = np.concatenate((y0_species,y0_resources),axis=None)
time = np.linspace(0,10000,10000) 

#simulate
res = model.time_course(time,y0)
res = np.array(res)

#visualize
fig = plt.figure(figsize=(15,8))
ax = plt.subplot(111)
for i in range(number_species):
    ax.plot(time,res[:,i], label = "species %s" % str(i+1))
ax.tick_params(labelsize=20)
plt.ylim(10e-10,10e5)
plt.xscale('log')
plt.xlabel('Time [a.u.]', fontsize=20)
plt.yscale('log')
plt.ylabel('Consumer.Pop $N_i$', fontsize=20)
#plt.savefig("l_0_8_Consumers100_Resources_30.png", format="png")
plt.show()


#steadystate and test if values stay constant
fig = plt.figure(figsize=(15,8))
ss = model.steadystate(y0,toleranz=1e-12)
res = model.time_course(time,ss)
res = np.array(res)
plt.plot(time,res)
plt.show()

#Experiment: Which resource preference leads to survival
Cs = []
steady_states = []
for i in range(5000):
    print(i)
    model.params['C'] = np.array([org_C*np.random.normal(1,0.3,5) for i in range(5)])
    ss = model.steadystate(y0,toleranz=1e-12)
    Cs.append(model.params['C'])
    steady_states.append(ss)

met = ['Glc','Xyl','Gly','Ac','Lac']
idx = [np.where(i[:5]>1)[0] for i in steady_states]

max_met = []
for i in range(len(idx)):
    tmp=Cs[i][idx[i],:]
    for j in tmp:
        max_met.append(met[np.argmax(j)])
#%%
fig = plt.figure(figsize=(15,8))    
plt.rc('axes',labelsize=20)
plt.rc('xtick',labelsize=20)
plt.rc('ytick',labelsize=15)
sns.countplot(max_met,order=['Glc','Xyl','Lac'])
plt.yscale('log')
plt.savefig("E.coli_subtypes.png")

#%% E.coli and Shigella community

model = Mcrm()
n_resources = 5
n_species = 10
org_C = np.array([0.37,0.3,0.08,0.09,0.16])
org_C_shi = np.array([0.57 , 0.31 , 0.027, 0.   , 0.096])
exp_scheme = ["E.coli","E.coli","E.coli","E.coli","E.coli","S.boydii","S.boydii","S.boydii","S.boydii","S.boydii"]

#set metaparams
model.change_metaparams({'number_species':n_species,'number_resources':n_resources,
                        'h':'renewable', 'sigma': 'type I'})

number_species = model.metaparams['number_species']
number_resources = model.metaparams['number_resources']

#set parameters
model.params['l'] = np.array([0.321,0.329,0.545,0.471,0.4])
model.params['D'] = np.array([[0,0,0,0,0],[0,0,0,0,0],[0.35,0.8,0,0.58,0.46],[0.4,0.12,0.6,0,0.54],[0.25,0.081,0.4,0.42,0]])
model.params['D'] = np.array([model.params['D'] for i in range(number_species)])
e_colis = np.array([org_C*np.random.normal(1,0.3,5) for i in range(5)])
s_bodyiis = np.array([org_C_shi*np.random.normal(1,0.3,5) for i in range(5)])
model.params['C'] = np.concatenate((e_colis,s_bodyiis),axis=0)
model.params['k'] = np.zeros(number_resources)
model.params['k'][0] = 10e4
model.params['k'][1] = 10e4

#%%
#initial values
y0_resources = np.zeros(number_resources)
y0_resources[0] = 1e2
y0_resources[1] = 1e2
y0_species = np.repeat(10e3,number_species)
y0 = np.concatenate((y0_species,y0_resources),axis=None)
time = np.linspace(0,10000,10000) 

#simulate
res = model.time_course(time,y0)
res = np.array(res)

#visualize
fig = plt.figure(figsize=(15,8))
ax = plt.subplot(111)
for i in range(number_species):
    ax.plot(time,res[:,i], label = "species %s" % str(i+1))
ax.tick_params(labelsize=20)
plt.ylim(10e-10,10e5)
plt.xscale('log')
plt.xlabel('Time [a.u.]', fontsize=20)
plt.yscale('log')
plt.ylabel('Consumer.Pop $N_i$', fontsize=20)
#plt.savefig("l_0_8_Consumers100_Resources_30.png", format="png")
plt.show()


#steadystate and test if values stay constant
fig = plt.figure(figsize=(15,8))
ss = model.steadystate(y0,toleranz=1e-12)
res = model.time_course(time,ss)
res = np.array(res)
plt.plot(time,res)
plt.show()


Cs = []
steady_states = []
for i in range(5000):
    print(i)
    e_colis = np.array([org_C*np.random.normal(1,0.3,5) for i in range(5)]) 
    s_bodyiis = np.array([org_C_shi*np.random.normal(1,0.3,5) for i in range(5)])
    model.params['C'] = np.concatenate((e_colis,s_bodyiis),axis=0)
    ss = model.steadystate(y0,toleranz=1e-12)
    Cs.append(model.params['C'])
    steady_states.append(ss)

#Experiment: Which resource preference leads to survival
met = ['Glc','Xyl','Gly','Ac','Lac']
idx = [np.where(i[:10]>1)[0] for i in steady_states]
species = np.concatenate([np.array(exp_scheme)[i] for i in idx],axis=None)
max_met = []
for i in range(len(idx)):
    tmp=Cs[i][idx[i],:]
    for j in tmp:
        max_met.append(met[np.argmax(j)])
        

fig = plt.figure(figsize=(15,8))    
plt.rc('axes',labelsize=20)
plt.rc('xtick',labelsize=20)
plt.rc('ytick',labelsize=15)
plt.rc('legend',fontsize=20)
plt.legend(["E.coli","S.boydii"])
sns.countplot(max_met,hue=species)
plt.yscale('log')
#plt.savefig("E.coli_S.boydii.png")






