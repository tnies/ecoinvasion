#!/bin/bash
# This file makes the repetition of Invasion experiments easy

dir_name='Experiment_InvasionNormInv' #dir for saving the produced data sets
nExp=25 #specifies the number of experiments 
mkdir $dir_name


for i in $(seq 1 $nExp); do 
current_dir=$dir_name/'Invasion_'$(($i))
mkdir $current_dir 
python Invasion2.py #python file containing the code for invasion experiment 
mv *.p $current_dir
mv *.png $current_dir
done
