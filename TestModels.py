# -*- coding: utf-8 -*-
"""
This file does not contain any real analysis. It is only for testing the model
"""
                
from models import Mcrm 
import matplotlib.pyplot as plt
import numpy as np
from utils import getConsumerMatrix, getMetabolismMatrix


__author__ = 'Tim Nies'
__credit__='Tim Nies'
__liscence__='GPLv3'
__vesion__='0.1'
__maintainer__='Tim Nies'


#%%

model = Mcrm()
model.change_metaparams({'number_species':50,'number_resources':60})
model.change_metaparams({'h':'renewable', 'sigma': 'type I'})
number_species = model.metaparams['number_species']
number_resources = model.metaparams['number_resources']

#y0 = np.random.rand(number_species+number_resources)
#y0_resources = np.random.rand(number_resources) +10e4
y0_resources = np.zeros(number_resources)
#y0_resources[0] = 10000
y0_species = np.repeat(10e3,number_species)
y0 = np.concatenate((y0_species,y0_resources),axis=None)
time = np.linspace(0,10000,10000) #time


#%%

model.params['C'] = getConsumerMatrix(model)
model.params['D'] = getMetabolismMatrix(model)
#model.params['D'] = np.array([[0.2,0.8],[0.2,0.8]])
#model.params['C'] = np.array([[3,0],[0,2],[0,1],[0,0],[0,0]])



#%%
#model.params['k'] = [10e4,0]
model.params['k'] = np.zeros(number_resources)
model.params['k'][0] = 10e4
#%%
model.params['l'] = np.repeat(0,number_resources)

#%%

res = model.time_course(time,y0)
res = np.array(res)

#%%
#fig = plt.figure(figsize=(20,12))
ax = plt.subplot(111)
for i in range(number_species):
    ax.plot(time,res[:,i], label = "species %s" % str(i+1))
ax.tick_params(labelsize=20)
plt.ylim(10e-10,10e5)
plt.xscale('log')
plt.xlabel('Time', fontsize=20)
plt.yscale('log')
plt.ylabel('Consumer.Pop $N_i$', fontsize=20)
#plt.savefig("high_leakage_l_0_0001_Consumers.svg", format="svg")
plt.show()


#%%

#fig = plt.figure(figsize=(20,12))
ax = plt.subplot(111)
for i in range(number_resources):
    ax.plot(time[:200],res[:200,number_species+i], label = "resource %s" % str(i+1))
ax.tick_params(labelsize=20)
#plt.ylim(10e-10,10e5)
plt.xscale('log')
plt.xlabel('Time', fontsize=20)
plt.yscale('log')
plt.ylabel('Resources Con. $R_a$', fontsize=20)
#plt.savefig("high_leakage_l_0_0001_Resources.svg", format="svg")
plt.show()
