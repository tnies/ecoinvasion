#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Calculate the leakage parameter for different resources
"""

import numpy as np
import cobra as cb
import matplotlib.pyplot as plt
import seaborn as sns
import gsmtools

__author__ = 'Tim Nies'
__credit__='Tim Nies'
__liscence__='GPLv3'
__vesion__='0.1'
__maintainer__='Tim Nies'

#load the ecoli and the yeast model
ecoli_model = cb.io.load_json_model('/home/tim/Desktop/Bacgrowth/GMmodels/BIGGdb/iJO1366.json')

#delete predidefined carbon source
ecoli_model.reactions.EX_glc__D_e.bounds=(0,0)

# Define carbon sources and influx range
carbon_sources = ['EX_glc__D_e','EX_xyl__D_e','EX_gly_e','EX_ac_e','EX_lac__D_e']
influx_range = np.linspace(1,50,100)

#%%

l_dict = {}
for i in carbon_sources:
     values = []
     for j in influx_range:
         values.append(gsmtools.get_carbon_fraction_in_anabolism(ecoli_model,i,(-j,0)))
     l_dict[i] = values

#%%
plt.figure(figsize=(15,12))
for k in l_dict:
    plt.plot(influx_range,1-np.array(l_dict[k]), linewidth=5,label=k)
plt.xlabel(r'Influx range $\frac{mmol}{gDW * h}$',size=25)
plt.ylabel('Catabolic carbon fraction',size=25)
plt.tick_params(labelsize=25)
plt.legend(prop={'size':25})
plt.savefig('catabolic_carbon_fraction.png')
plt.show()
