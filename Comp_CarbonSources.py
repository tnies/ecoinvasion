#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Analysis of Glucose, Xylose, Arabinose, Glycine, Acetate and Lacatet as carbon
source in the e.coli model iJO1366 and in the yeast model iMM904
"""

import numpy as np
import cobra as cb
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import gsmtools

__author__ = 'Tim Nies'
__credit__='Tim Nies'
__liscence__='GPLv3'
__vesion__='0.1'
__maintainer__='Tim Nies'

#load the ecoli and the yeast model
#ecoli_model = cb.io.load_json_model('/home/tim/Desktop/Bacgrowth/GMmodels/BIGGdb/iJO1366.json')
#shigella_model = cb.io.load_json_model('/home/tim/Desktop/Bacgrowth/GMmodels/BIGGdb/iSBO_1134.json')


ecoli_model = cb.io.read_sbml_model('/home/tim/Desktop/MicrobialPop/GSMs/BiGG/iJO1366.sbml')
shigella_model = cb.io.read_sbml_model('/home/tim/Desktop/MicrobialPop/GSMs/BiGG/iSBO_1134.sbml')


#delete predidefined carbon source
ecoli_model.reactions.EX_glc__D_e.bounds=(0,0)
shigella_model.reactions.EX_glc__D_e.bounds=(0,0)

#delete maintenance
#ecoli_model.reactions.ATPM.bounds=(0,0)
#yeast_model.reactions.ATPM.bounds=(0,0)


model = [ecoli_model,shigella_model]

# Define carbon sources and influx range
carbon_sources = ['EX_glc__D_e','EX_xyl__D_e','EX_gly_e','EX_ac_e','EX_lac__D_e']
met = ['Glucose','Xylose','Glycine','Acetate','Lactate']
influx_range = np.linspace(1,50,100)

#%%
###############################################################################
###############################################################################
#Analysis Ecoli
###############################################################################
###############################################################################

#####ROBUSTNESS ANALYSIS#####

idx_ex_ecoli = gsmtools.get_exchange_reactions([i.id for i in ecoli_model.reactions]) 
r_analyses = {i+'_C_source':gsmtools.robustness_analysis(ecoli_model,i,influx_range) for i in carbon_sources}

#loop to get all non zero exchange fluxes
in_out_ecoli = {}
norm_in_out_ecoli = {}
for i in r_analyses:
    analysis = r_analyses[i]
    tmp = analysis[analysis.columns[idx_ex_ecoli]]
    in_out_ecoli[i] = tmp[tmp.columns[tmp.sum()!=0]]
    norm_in_out_ecoli[i] = in_out_ecoli[i].div(-in_out_ecoli[i][i.split('_C')[0]],axis='rows')

analyses = {'non_norm':in_out_ecoli,'norm':norm_in_out_ecoli}


fig, axes = plt.subplots(5, 2,figsize=(15,20))
for j,k in enumerate(analyses):
    tmp = analyses[k]
    for l,m in enumerate(tmp):
        axes[l,j].plot(influx_range,tmp[m])
        axes[l,j].set_xlabel(r'Carbon source influx $\frac{mmol}{gDW * h}$')
        if j == 0:
            axes[l,j].set_ylabel(r'Metabolite exchange flux $\frac{mmol}{gDW * h}$')
        else:
            axes[l,j].set_ylabel(r'Metabolite exchange $\frac{mmol}{mmol Carbon Source}$')
        axes[l,j].set_title(k +'_'+ m)
plt.tight_layout()
plt.show()

#%%

#####MAXIMUM YIELD D-MATRIX#####

maximaloutput = gsmtools.get_proto_metabolism_by_optimization(ecoli_model,carbon_sources,'BIOMASS_Ec_iJO1366_core_53p95M',source_bounds=(-10,-10), f=0.1).T
maximaloutput = maximaloutput/maximaloutput.sum()
maximaloutput = maximaloutput.fillna(0)

#%%
maximaloutput.columns = met
maximaloutput.index = met


fig,ax = plt.subplots(figsize=(15,12))
sns.heatmap(maximaloutput,annot=True,cmap='coolwarm', annot_kws={'fontsize':30},vmin=0,vmax=1)
cax = plt.gcf().axes[-1]
cax.tick_params(labelsize=30)
plt.xticks(size=25,rotation=90)
plt.yticks(size=25,rotation=360)
plt.savefig('D_modFBA.png')
plt.show()



#%%

maximaloutput2 = gsmtools.get_proto_metabolism(ecoli_model,carbon_sources, number_of_sample=20000)
maximaloutput2 = pd.DataFrame(maximaloutput2).T*-1

#%%

met = ['Glucose','Xylose','Glycine','Acetate','Lactate']
maximaloutput2.columns = met
maximaloutput2.index = met

fig,ax = plt.subplots(figsize=(15,12))
sns.heatmap(maximaloutput2,annot=True,cmap='coolwarm', annot_kws={'fontsize':30}, vmin=0,vmax=1)
cax = plt.gcf().axes[-1]
cax.tick_params(labelsize=30)
plt.xticks(size=25,rotation=90)
plt.yticks(size=25,rotation=360)
plt.savefig('D_sampling.png')
plt.show()

#%% C matrix

blah, C = gsmtools.estimate_C_matrix(ecoli_model,carbon_sources,'BIOMASS_Ec_iJO1366_core_53p95M', number_species=5,s=0.3)
C = pd.DataFrame(C)
C.columns = met
C.index = ['Species1','Species2','Species3','Species4','Species5']

fig,ax = plt.subplots(figsize=(15,12))
sns.heatmap(C,annot=True,cmap='coolwarm', annot_kws={'fontsize':30}, vmin=0,vmax=1)
cax = plt.gcf().axes[-1]
cax.tick_params(labelsize=30)
plt.xticks(size=25,rotation=90)
plt.yticks(size=25,rotation=360)
plt.savefig('C_matrix.png')
plt.show()

#%% E.coli and Shigella 

C = gsmtools.community_C_matrix(model,biomass_reaction=['BIOMASS_Ec_iJO1366_core_53p95M','BIOMASS_Ec_iJO1366_core_53p95M'],carbon_sources=carbon_sources)
C = pd.DataFrame(C)
C.columns = met
C.index = ['E.coli','S.bodyii']

fig,ax = plt.subplots(figsize=(15,12))
sns.heatmap(C,annot=True,cmap='coolwarm', annot_kws={'fontsize':30}, vmin=0,vmax=1)
cax = plt.gcf().axes[-1]
cax.tick_params(labelsize=30)
plt.xticks(size=25,rotation=90)
plt.yticks(size=25,rotation=360)
plt.savefig('C_matrix_community.png')
plt.show()
