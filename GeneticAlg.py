#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Contains the Genetic Algorithm necessary for calculating invader preferences.
Inspired by the tutorial https://github.com/MorvanZhou/Evolutionary-Algorithm 
from Morvan Zhou
"""

import numpy as np
from numba import jit

__author__ = 'Tim Nies'
__credit__='Tim Nies'
__liscence__='GPLv3'
__vesion__='0.1'
__maintainer__='Tim Nies'


DNA_SIZE = 10
POP_SIZE = 5
CROSS_RATE = 0.8
MUTATION_RATE = 0.12 
N_GENERATIONS = 80
X_BOUND = [0,1]


def F(x,invasion_model,invasion_y0,original_C):
    """Function to be optimized"""
    C = original_C
    C2 = np.vstack((C,x))
    invasion_model.params['C'] = C2
    invasion_fitness = invasion_model.system(0,invasion_y0)[invasion_model.metaparams['number_species']-1]
    if invasion_fitness<0:
        return 1e-12
    return invasion_fitness


def get_fitness(pred):
    """Fitness function"""
    return pred - min(pred) + 1e-10



def translateDNA(pop,normalize=False,threshold=1):
    """Translates binary population in real numbered population """
    translated = pop.dot(2 ** np.arange(DNA_SIZE)[::-1]) / float(2**DNA_SIZE-1) * X_BOUND[1]
    if normalize == False:
        return translated
    else:
        return translated/np.sum(translated,axis=0)*threshold



def select(pop, fitness):
    """Selection function"""
    idx = np.random.choice(np.arange(POP_SIZE), size=POP_SIZE, replace = True, p = fitness/fitness.sum())
    return pop[idx]



def crossover(parent, pop):
    """Defines Crossover"""
    if np.random.rand() < CROSS_RATE:
        i_ = np.random.randint(0,POP_SIZE,size=1)
        cross_points = np.random.randint(0,2, size=DNA_SIZE).astype(np.bool)
        parent[cross_points] = pop[i_, cross_points]
    return parent



def mutate(child):
    """Defines mutation"""
    for point in range(DNA_SIZE):
        if np.random.rand() < MUTATION_RATE:
            child[point] = 1 if child[point] == 0 else 0
    return child



@jit(parallel=True)
def GA_analysis(Meta_pop_size,invasion_model,invasion_y0,original_C,threshold=False):
    """
    Genetic Algorithms to find an arbitrary number of possible invaders"""
    number_resources = invasion_model.metaparams['number_resources']
    number_species = invasion_model.metaparams['number_species']
    meta_org_C = []
    meta_fitness_log = []
    meta_F_log = []
    meta_matrices = []
    meta_steadystates = []
    pop_reaction = []
    for _ in range(Meta_pop_size):
        #model.params["D"] = [model.getMetabolismMatrix()] #is it in one or in all models ?
        print(_)
        pop = np.random.randint(2,size=(1*number_resources,POP_SIZE,DNA_SIZE))
        fitness_log = []
        F_log = []
        for k in range(N_GENERATIONS):
            #Evaluate
            if threshold:
                translated = translateDNA(pop,normalize=True,threshold=threshold)
            else:
                translated = translateDNA(pop)
            matrices = translated.T.reshape(POP_SIZE,1,number_resources)
            F_values = []
            for i in matrices:
                F_values.append(F(i,invasion_model,invasion_y0,original_C))
            F_log.append(F_values)
            fitness = get_fitness(np.array(F_values))
            fitness_log.append(fitness)
            #best_point = [translateDNA(i[np.argmax(fitness),:]) for i in pop]
    
            #Evolve
            for j in range(len(pop)):
                pop_x = select(pop[j], fitness)
                pop_x_copy = pop_x.copy()
                for parent in pop_x_copy:
                    child = crossover(parent, pop_x_copy)
                    child = mutate(child)
                    parent[:] = child
                    pop[j] = pop_x
        
        steadystates = invasion_model.steadystate(invasion_y0)
        meta_org_C.append(invasion_model.params["C"])    
        meta_matrices.append(matrices[0])
        meta_fitness_log.append(fitness_log)
        meta_F_log.append(F_log)
        meta_steadystates.append(steadystates)
        pop_reaction.append(steadystates[:number_species-1] - invasion_y0[:number_species-1])
    return pop_reaction, meta_steadystates, meta_org_C, meta_matrices, meta_fitness_log, meta_F_log

__all__ = ['GA_analysis']