<h1>ecoinvasion</h1>

Analyzing microbial ecosystems using an extended MacArthur's consumer resource
system. 

This repository contains code that was developed for the master's thesis superived by Prof. Dr. Oliver Ebenhöh
with the title: "**Extending MacArthur's Consumer-Resource Models by Invasion Analysis 
and Parameter Approximation from Constraint-based Models**."

During the process of this master's thesis it was attempted to synthesize differnt
modelling approaches. This includes:

+ *MacArthus's consumer resorce models*
+ *Constraint-based models* and *Metabolic networks*
+ *Adaptive Dynamics*
+ *Metabolic black box models*

To replicate the analyses folowing files are essential:

+ TestModels.py (Figure 3.1 and 3.2)
+ reproduceMA.py (Figure 3.3)
+ MacArthur1969.py (Figure 3.4-6)
+ properties_microcoms (Figure 3.8-9)
+ Invasion2,  Analysis\_Experimental\_Invasion.py, Population_reaction_experiment.sh (Figure 3.10-12)
+ SequentialInvasion (Figure 3.13-16)
+ BiomassEnergyOfFormation.py (Figure 3.17-18)
+ Comp_CarbonSources.py (Figure 3.19-23)
+ TestofPracticability.py (Figure 3.24-25)

<h2>Contact</h2>
Quantitative und Theoretische Biologie<br>
Heinrich-Heine-Universität Düsseldorf<br>  
Building: 22.07.00.031<br> 
Postbox: ZSL 006<br>
Universitätsstraße 1<br>  
40225 Düsseldorf<br>   

<h2>Important References</h2>

Marsland III, Robert, et al. "Available energy fluxes drive a transition in the diversity, stability, and functional structure of microbial communities." PLoS computational biology 15.2 (2019): e1006793.


Geritz, Stefan AH, Géza Mesze, and Johan AJ Metz. "Evolutionarily singular strategies and the adaptive growth and branching of the evolutionary tree." Evolutionary ecology 12.1 (1998): 35-57.
 
 
Battley, Edwin H. "Calculation of entropy change accompanying growth of Escherichia coli K‐12 on succinic acid." Biotechnology and bioengineering 41.4 (1993): 422-428.
