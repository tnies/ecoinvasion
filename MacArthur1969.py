#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Simple implementation of an model proposed by MacArthur in his paper "Species 
Packing and what interspecies competition minimizes" (1969).
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
from functools import partial

__author__ = 'Tim Nies'
__credit__='Tim Nies'
__liscence__='GPLv3'
__vesion__='0.1'
__maintainer__='Tim Nies'

#%%######################################################################
#########################################################################
############################# MacArthur1969 #############################
#########################################################################
#########################################################################

#Species parameters
c = np.array([1.,1.]) #governing transfer from resource to species

T = np.array([0.2,0.2]) #maintenance

a = np.array([[1,0], #a_ij matrix describes probality per unit time that species i encounters resource j and eats it
              [0,1]])

#Resource parameters
r = np.array([1.,1.]) #intrinsic rate of natural increase ?

K = np.array([1.,1]) #carrying capicity of resources in the habitate

w = np.array([1.,1.]) #weights


#make parameter dictionary
param = {'c':c,'w':w,'T':T,'a':a,'r':r,'K':K,'n_species':2}


#define system
def system(y,t,c,w,T,a,r,K,n_species):
    dx_dt = c*y[:n_species]*(np.sum(w*a*y[n_species:],axis=1)-T)
    dR_dt = (r*y[n_species:]/K)*[K-y[n_species:]]-(a*y[n_species:]).T.dot(y[:n_species])
    return np.concatenate((dx_dt,dR_dt),axis=None)


#integrate system
y = np.array([1, 1, 1, 1])
t = np.linspace(0,200,100)
res = odeint(partial(system,**param),y,t)
plt.plot(t,res,linewidth=3)

# Change parameters systematically
scan_range = [0.1,0.2,0.3,0.5,1,1.5,2]

def param_scan(parameter,initial_param,scan_range):
    """creates a list of dictionaries with changing parameter"""
    scan = []
    for i in scan_range:
        org = param.copy()
        org[parameter] = org[parameter]*i
        scan.append(org)
    return scan

########################################################################
#The matrices below classifies species into generalists and specialists
########################################################################
    
#two specialists
#a_new = np.array([[1.,0.],
#                  [0.,1.]])

##two generalists
#a_new = np.array([[0.5,0.5], 
#                  [0.5,0.5]])
    
##one generalists and one specialist
a_new = np.array([[0.5,0.5], 
                  [1.,0.]])
    
param['a'] = a_new

fig, axes = plt.subplots(5, 4,figsize=(15,20))
for g,h in enumerate(['c','w','T','K','r']):
    scan = param_scan(h,param,scan_range)
    for i in scan:
        res = odeint(partial(system,**i),y,t)
        for k,j in enumerate(range(4)): 
            axes[g,k].plot(t,res[:,j],linewidth=3,label = np.round(i[h][0],2))
            axes[g,k].legend()
            axes[g,k].set_xlabel('Time [a.u.]',fontsize=15)
            axes[g,k].set_ylabel('Abundance [a.u.]',fontsize=15)
            if k in [0,1]:
                axes[g,k].set_title(('Parameter %s: Species %d' % (h,k+1)),size=15)
            else:
                axes[g,k].set_title(('Parameter %s: Resource %d' % (h,k-1)),size=15)
plt.tight_layout()
#plt.savefig('MacArthur1969_OneSpecialist_oneGeneralist.svg',format='svg')
#plt.savefig('MacArthur1969_OneSpecialist_oneGeneralist.png',format='png')
plt.show()

#%%######################################################################
#########################################################################
############################# Mrasland2019 ##############################
#########################################################################
#########################################################################

#Species parameters
g = np.array([1.,1.]) #governing transfer from resource to species

m = np.array([0.2,0.2]) #maintenance

c = np.array([[1,0], #c_ij resource preference of species i (is the same as a_ij in M1969 ?)
              [0,1]])

#Resource parameters
r = np.array([1.,1.]) #intrinsic rate of natural increase ?

K = np.array([1.,1]) #carrying capicity of resources in the habitate

w = np.array([1.,1.]) #weights

D = np.array([[1,0], #metabolism
              [0,1]])
    
l = np.array([0.5,0.5])

param = {'g':g, 'm':m, 'c':c, 'r':r, 'K':K, 'w':w, 'D':D, 'l':l, 'n_species': 2}

def system2(y,t,g,m,c,r,K,w,D,l,n_species):
    dN_dt = g*y[:n_species]*(np.sum((1-l)*w*c*y[n_species:],axis=1)-m)
    dR_dt = (r*y[n_species:]/K)*(K-y[n_species:]) - (c*y[n_species:]).T.dot(y[:n_species]) + \
        np.sum((c*y[n_species:]).T.dot(y[:n_species])*D*l,axis=1) #the term wa/wb cancelled out because all w are assumed to be 1 
    return np.concatenate((dN_dt,dR_dt),axis=None)

#integrate system
y = np.array([1, 1, 1, 1])
t = np.linspace(0,200,100)
res = odeint(partial(system2,**param),y,t)
plt.plot(t,res,linewidth=3)

# Change parameters systematically

########################################################################
#The matrices below classifies species into generalists and specialists
########################################################################

##two specialists
#c_new = np.array([[1,0], 
#                  [0,1]])
   
##two generalists
#c_new = np.array([[0.5,0.5], 
#                  [0.5,0.5]])
    
#one generalists and one specialist
c_new = np.array([[0.5,0.5], 
                  [1.,0.]])
    
D_new = np.array([[0,1], 
                [1,0]]) #sum axis=0 -> 1

#D_new = np.array([[0.5,0.5], 
#                  [0.5,0.5]]) #sum axis=0 -> 1    

# D_new = np.array([[0.9,0.5], 
#                  [0.1,0.5]]) #sum axis=0 -> 1
    
#D_new = np.array([[0.9,0.8], 
#                  [0.1,0.2]]) #sum axis=0 -> 1
#    
param['c'] = c_new
param['D'] = D_new

fig, axes = plt.subplots(6, 4,figsize=(15,20))
for g,h in enumerate(['g','m','r','K','w','l']):
    scan = param_scan(h,param,scan_range) #parameter scan is defined in line 59
    for i in scan:
        res = odeint(partial(system2,**i),y,t)
        for k,j in enumerate(range(4)): #0 = species 1, 2 = resource 1
            axes[g,k].plot(t,res[:,j],linewidth=3,label = np.round(i[h][0],2))
            axes[g,k].legend()
            axes[g,k].set_xlabel('Time [a.u.]',fontsize=15)
            axes[g,k].set_ylabel('Abundance [a.u.]',fontsize=15)
            if k in [0,1]:
                axes[g,k].set_title(('Parameter %s: Species %d' % (h,k+1)),size=15)
            else:
                axes[g,k].set_title(('Parameter %s: Resource %d' % (h,k-1)),size=15)
plt.tight_layout()
plt.savefig('Marsland2019_oneSpecialist_oneGeneralist_D09050105.svg',format='svg')
plt.savefig('Marsland2019_oneSpecialist_oneGeneralist_D09050105.png',format='png')
plt.show()