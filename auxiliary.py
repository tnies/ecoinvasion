# -*- coding: utf-8 -*-
"""
This file contains some auxiliary functions for the extended MacArthur's con-
sumer resource model
"""
from parameters import paramList
import numpy as np

__author__ = 'Tim Nies'
__credit__='Tim Nies'
__liscence__='GPLv3'
__vesion__='0.1'
__maintainer__='Tim Nies'


#%%


class auxiliary_functions(paramList):
    def __init__(self):
        super(auxiliary_functions,self).__init__()
        
        self.sigma = {'type I':      self.Sigma_I, 
                      'type II':     self.Sigma_II,
                      'type III':    self.Sigma_III}
        #describes dynamics of resource when organismic interaction is absent              
        self.h = {'renewable':      self.h_renew, 
                  'non_renewable':  self.h_non_renew,
                  'off':            self.h_zero
        }
        
    def Sigma_I(self,R):
       return self.params['C']*R
        
    def Sigma_II(self,R):
        return self.params['C']*R/(1+self.params['C']*R/self.params['sigma_max'])
        
    def Sigma_III(self,R):
        return (self.params['C']*R)**self.params['n']/(1+self.params['C']*R/self.params['sigma_max'])**self.params['n']
        
    def h_renew(self,R):
        return self.params['k']-(R/self.params['tau'])
        
    def h_non_renew(self,R):
        return self.params['r']*R*(self.params['K']-R)
        
    def h_zero(self,R): #It coult be doubted that this is useful or at least I cannot see its function
        return 0
        
    def J_in(self,R):
        return self.params['w']*self.sigma[self.metaparams['sigma']](R)
    
        
    def J_growth(self,R):
        return (1-self.params['l'])*self.J_in(R)

#    def J_out(self,R):
#        return (self.params['l']*self.J_in(R)).dot(self.params['D'].T)
    
    #multiple metabolisms
    def J_out(self,R):
        return self.params['l']*np.einsum('ib,iab -> ia', self.J_in(R), self.params["D"])
        
                
#    def get_Number_species(self):
#        return self.metaparams['number_species']
    
    
    
    


        



        
    