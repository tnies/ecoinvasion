#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Analysis of the sequential invasion in ecosystems described by the extended
MacArthur's consumer resource model (Marsland et al. 2019)
"""

import pickle
import numpy as np
import matplotlib.pyplot as plt
from models import Mcrm
from plot_routines import Plot_SequentialInvasion1,Plot_SequentialInvasion2
from utils import  invasion_time_course, ShannonEntropy
from matplotlib.ticker import MaxNLocator

__author__ = 'Tim Nies'
__credit__='Tim Nies'
__liscence__='GPLv3'
__vesion__='0.1'

#%%

#Begin with prespecified invasion models. Invasive species were found via genetic
#algorithmc procedure

data_dir = '/home/tim/Desktop/Bacgrowth/Git/ecomodels/Models/data/Experiment_Invasion'
meta_matrices = 'Invasion_1000points_40s30r_sameD_metamatrices.p'
i_ss = 'Invasion_1000points_40s30r_sameD_org_ss.p'
model = 'model.p'


invaders = pickle.load(open(data_dir+'/Invasion_9/'+meta_matrices,'rb'))
imodel_params = pickle.load(open(data_dir+'/Invasion_9/'+model,'rb'))
ss = pickle.load(open(data_dir+'/Invasion_9/'+i_ss,'rb'))

#Create model
params,meta_params = imodel_params
model = Mcrm()
model.change_metaparams(meta_params)
model.params = params

#%%############################################################################
# ONE SEQUENTIAL INVASION EXPERIMENT
###############################################################################

#Reshape invaders
invaders = np.reshape(invaders,(1000,30))

#sequential invasion
res, t, species_count, dts, seqC, yl = invasion_time_course(model,1000,ss,invaders,40,15)

idx = [res[i][:species_count[i]] > 10 for i in range(len(res))]
living_species = [sum(idx[i]) for i in range(len(idx))]


Plot_SequentialInvasion1(res,t,species_count,save=True)
Plot_SequentialInvasion2(res,t,species_count,save=True)

pickle.dump(res, open('res_single_seqInv.p','wb'))
pickle.dump(t, open('t_single_seqInv.p','wb'))
pickle.dump(species_count, open('species_count_single_seqInv.p','wb'))
pickle.dump(seqC,open('seqC_single_seqInv.p','wb'))


#%%############################################################################
# MULTIPLE SEQUENTIAL INVASION EXPERIMENT
###############################################################################

ress = []
ts = []
species_counts = []
seqCs = []

for _ in range(20):
    res, t, species_count, dts, seqC, yl = invasion_time_course(model,4500,ss,invaders,40,15)
    ress.append(res)
    ts.append(t)
    species_counts.append(species_count)
    seqCs.append(seqC)

#%% OPEN DATA WHEN NECESSARY

# ress = pickle.load(open('/home/tim/Desktop/Bacgrowth/Texte/VariantB/ress.p','rb'))
# ts = pickle.load(open('/home/tim/Desktop/Bacgrowth/Texte/VariantB/ts.p','rb'))
# species_counts = pickle.load(open('/home/tim/Desktop/Bacgrowth/Texte/VariantB/species_counts.p', 'rb'))
# seqCs = pickle.load(open('/home/tim/Desktop/Bacgrowth/Texte/VariantB/seqCs.p','rb'))
#%% TRANSFORM AND EXTRACT
    
    
    
living_species = []
Entropy = []
for j in range(len(ress)):
    print(j)
    idx = [ress[j][i][:species_counts[j][i]] > 10 for i in range(len(ress[j]))]
    living_species.append([sum(idx[k]) for k in range(len(idx))])
    #Entropy.append([np.mean(ShannonEntropy(seqCs[j][:species_counts[j][l]][idx[l]])) for l in range(len(idx))])

    
    
#%% PLOT


fig, ax = plt.subplots(figsize=(15,12))
ax.yaxis.set_major_locator(MaxNLocator(integer=True))
for i in range(20):
    plt.plot(ts[i],living_species[i],linewidth=5)
plt.xlabel('Time [a.u.]',fontsize=25)
plt.ylabel('Number of living species',fontsize=25)
plt.tick_params(labelsize=25)
#plt.savefig('SequentialInvasion_livingSpecies_CB_norm_100.png',format='png')
plt.show()

#%%

plt.figure(figsize=(15,12))
for i in range(20):
    plt.plot(ts[i],Entropy[i],linewidth=5)
plt.xlabel('Time [a.u.]',fontsize=25)
plt.ylabel('Shannon Entropy',fontsize=25)
plt.tick_params(labelsize=25)
plt.savefig('MeanShannonEntropy_CB_100.png', format='png')
plt.show()

#%%

import pickle

pickle.dump(ress, open('ress.p','wb'))
pickle.dump(ts, open('ts.p','wb'))
pickle.dump(species_counts, open('species_counts.p','wb'))
pickle.dump(seqCs,open('seqCs.p','wb'))
