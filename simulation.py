# -*- coding: utf-8 -*-
"""
This file contains simulation routines
"""

import numpy as np
from scipy.integrate import ode
from assimulo.problem import Explicit_Problem
from assimulo.solvers import CVode

__author__ = 'Tim Nies'
__credit__='Tim Nies'
__liscence__='GPLv3'
__vesion__='0.1'
__maintainer__='Tim Nies'


class simulation_routines(object):
    def __init__(self):
        super(simulation_routines,self).__init__()
        
        
    def time_course(self,t,y0):
        Integrator = ode(self.system).set_integrator('lsoda').set_initial_value(y0,0)
        cnt = 1
        res = [y0]
        while cnt < len(t):
            sol = Integrator.integrate(t[cnt])
            res.append(sol)
            cnt += 1
        return res
    
    
    
    def time_course_assim(self,y0):
        mod = Explicit_Problem(self.system,y0,0.0)
        sim = CVode(mod)
        return sim
        
    
    
    def steadystate(self,x0,toleranz=1e-7):
        results0 = [x0]
        time=range(1000000000)
        error = np.linalg.norm(results0[0],ord=2)
        integrator = ode(self.system).set_integrator('lsoda').set_initial_value(x0,0)
        cnt=1
        while cnt < len(time) and error > toleranz:
            zahl = integrator.integrate(time[cnt])
            error=np.linalg.norm(zahl-results0[-1],ord=2)
            results0.append(zahl) 
            cnt += 1
        return np.round(results0[-1],decimals=5)
    
    
    
    def assim_steadystate(self,y0,tolerance=1e-12,max_t=100000):
        sim = self.time_course_assim(y0)
        sim.verbosity=50
        x0 = y0
        error = np.linalg.norm(x0,ord=2)
        i=0
        j = 10000
        while i < max_t and error > tolerance:
            x1 = sim.simulate(j,)[1][-1]
            error = np.linalg.norm(x0-x1, ord=2)
            x0 = x1
            j += 5000
            i +=1
        return np.round(x0, decimals=5)
    
    

        

