#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Analysis of general properties of microbial populations
"""

import pickle
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from models import Mcrm
from plot_routines import Plot_C
from utils import getConsumerMatrix,  getMetabolismMatrix



__author__ = 'Tim Nies'
__credit__='Tim Nies'
__liscence__='GPLv3'
__vesion__='0.1'
__maintainer__='Tim Nies'

#%% Basic model

model = Mcrm()
l_invasion = 0.8
n_resources = 30
n_species = 40

model.change_metaparams({'number_species':n_species,'number_resources':n_resources,
                        'h':'renewable', 'sigma': 'type I'})

number_species = model.metaparams['number_species']
number_resources = model.metaparams['number_resources']

#set parameters
model.params["C"] = getConsumerMatrix(model,normalize=False,specialists=False)
model.params["D"] = getMetabolismMatrix(model)
model.params['k'] = np.zeros(number_resources)
model.params['k'][0] = 10e4
model.params['l'] = np.repeat(l_invasion,number_resources)

#initial values
y0_resources = np.zeros(number_resources)
y0_resources[0] = 1e2
y0_species = np.repeat(10e3,number_species)
y0 = np.concatenate((y0_species,y0_resources),axis=None)

ss = model.steadystate(y0,toleranz=1e-12)

idx = np.where(ss[:40]>1)[0]
Plot_C(model.params['C'][idx,:],save=True)

#%% EXPERIMENT 1: SUM OF ELEMENTS

surv_diff = []
dead_diff = []
for _ in range(1000):
    print(_)
    model.params["C"] = getConsumerMatrix(model,normalize=False,specialists=False)
    model.params["D"] = getMetabolismMatrix(model)
    ss = model.steadystate(y0,toleranz=1e-12)
    sum_axis = np.sum(model.params['C'],axis=1)
    sum_axis = sum_axis/max(sum_axis)
    idx = np.where(ss[:40]>1)[0]
    idx_max = np.where(sum_axis == max(sum_axis))[0]
    try:
        idx = np.delete(idx,np.where(idx==idx_max)[0],axis=None)
    except:
        pass
    surv_diff.append(np.mean(sum_axis[idx]))
    dead_diff.append(np.mean(np.delete(sum_axis,idx)))

#%%

df1 = pd.concat([pd.DataFrame(surv_diff),pd.DataFrame(dead_diff)],axis=1)
df1.columns = ['Survivors','Dead']
pickle.dump(df1, open("Sum_differences.p", "wb" ) )

plt.figure(figsize=(15,12))
plt.hist(df1.T, bins=60, label=df1.columns)
plt.xlabel('Mean overall preference', size=30)
plt.ylabel('Counts', size=30)
plt.legend(prop={'size':25})
plt.xticks(fontsize=30)
plt.yticks(fontsize=30)
plt.savefig('PopulatioProperties_OverallPref_Surv_Dead.png')


#%% EXPERIMENT 2: LENGTH:

surv_len = []
dead_len = []

for _ in range(1000):
    print(_)
    model.params["C"] = getConsumerMatrix(model,normalize=False,specialists=False)
    model.params["D"] = getMetabolismMatrix(model)
    ss = model.steadystate(y0,toleranz=1e-12)
    len_axis = np.linalg.norm(model.params['C'],ord=2,axis=1)
    len_axis = len_axis/max(len_axis)
    idx = np.where(ss[:40]>1)[0]
    idx_max = np.where(sum_axis == max(sum_axis))[0]
    try:
        idx = np.delete(idx,np.where(idx==idx_max)[0],axis=None)
    except:
        pass
    surv_len.append(np.mean(len_axis[idx]))
    dead_len.append(np.mean(np.delete(len_axis,idx)))

#%%
df1 = pd.concat([pd.DataFrame(surv_len),pd.DataFrame(dead_len)],axis=1)
df1.columns = ['Survivors','Dead']
pickle.dump(df1, open("Sum_differences.p", "wb" ) )

plt.figure(figsize=(15,12))
plt.hist(df1.T, bins=60, label=df1.columns)
plt.xlabel('Mean Length', size=30)
plt.ylabel('Counts', size=30)
plt.legend(prop={'size':25})
plt.xticks(fontsize=30)
plt.yticks(fontsize=30)
plt.savefig('PopulatioProperties_Length_Surv_Dead.png')

            

