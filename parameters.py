#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Contains utilities for providing the standard parameters of the extended Mac-
Arthur's consumer resource model
"""

__author__ = 'Tim Nies'
__credit__='Tim Nies'
__liscence__=''
__vesion__='0.1'
__maintainer__='Tim Nies'


import numpy as np

class paramList(object):
    def __init__(self):
        super(paramList,self).__init__()
        self.metaparams = {'number_species':     2,
                           'number_resources':   10,
                           'sigma':              'type II',
                           'h':                  'renewable'
                           }
        

        self.params = self.make_params()
                  
                  
    def change_metaparams(self,change_dict):
        for i in change_dict:
            self.metaparams[i]=change_dict[i]
        self.params = self.make_params()
            
    def make_params(self):
        params = {'D':      np.eye(self.metaparams['number_resources']),
                  'C':      np.ones((self.metaparams['number_species'], self.metaparams['number_resources'])),
                  'w':      np.repeat(1,self.metaparams['number_resources']),
                  'l':      np.repeat(2,self.metaparams['number_resources']),
                  'm':      np.repeat(1, self.metaparams['number_species']),
                  'g':      np.ones(self.metaparams['number_species']),
                  'tau':    np.repeat(1, self.metaparams['number_resources']),
                  'k':      np.repeat(1, self.metaparams['number_resources']),
                  'K':      np.repeat(1, self.metaparams['number_resources']),
                  'r':      np.repeat(0.5, self.metaparams['number_resources']),
                  'sigma_max': 10,
                  'n':3
                  }
        return params
    
    

        