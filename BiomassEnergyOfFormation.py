#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Calculates energy of formation of biomass in the BIGG database
"""

import cobra as cb
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import collections
import gsmtools
import os

__author__ = 'Tim Nies'
__credit__='Tim Nies'
__liscence__='GPLv3'
__vesion__='0.1'
__maintainer__='Tim Nies'

directory = '/home/tim/Desktop/Bacgrowth/GMmodels/BIGGdb/'
#directory = '/home/tim/Desktop/MicrobialPop/GSMs/BiGG'
model_ids = os.listdir(directory)

analysis = {}

for i in model_ids:
    try:
        model = cb.io.load_json_model(directory + '/' + i)
        #model = cb.io.read_sbml_model(directory + '/' + i)
        biomass = gsmtools.get_biomass(model)
        print(i)
        for j in biomass:
            comp = biomass[j]
            comb_rxn,y,G_C,G_f = gsmtools.get_energy_of_formation_of_biomass(biomass[j])
            analysis[i+'_'+j] = {'Degree of reduction':y,'Energy of Combustion [KJ/C-mol]':G_C,
                    'Energy of formation [KJ/C-mol]': G_f}
    except:
        continue
#%%
analysis = pd.DataFrame(analysis).T

#%%

ax,fig = plt.subplots(figsize=(15,12))
sns.scatterplot(x='Degree of reduction',y='Energy of formation [KJ/C-mol]', data=analysis, color ='red', alpha=0.5,s=300)
plt.xlabel('Degree of reduction ($\gamma$)', fontsize=20)
plt.ylabel('Energy of formation $\Delta G_f$ [KJ/C-mol]',fontsize=20)
plt.xlim(3,8.5)
plt.ylim(-300,250)
plt.hlines(-70,0,8.5,linestyle='dashed')
plt.vlines(4.993068,-300,250,linestyles='dashed')
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
plt.annotate('Mean $\Delta G_f$',(3.2,-65), size=20)
plt.annotate('Mean $\gamma$',(5.05,205),rotation=90,size=20)
#plt.savefig('Energy_of_formation_BIGGdatabase.png')
plt.show()

#%%

biomass_fct = [i.rsplit('B')[1] for i  in analysis.index]
iJ_or_not = []
for i in biomass_fct:
    if 'iJO1366' in i:
        iJ_or_not.append('iJO1366')
    else:
        iJ_or_not.append('Others')
ax,fig = plt.subplots(figsize=(10,9))
sns.countplot(iJ_or_not)
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
plt.ylabel('Number of biomass functions', fontsize=20)
plt.xlabel('Original model', fontsize=20)
#plt.savefig('Comparison_of_biomass_function_origin')
plt.show()

#%%

fig, ax = plt.subplots(figsize=(12, 11), subplot_kw=dict(aspect="equal"))

data = list(collections.Counter(iJ_or_not).values())
names = ['iJO1366 (55.76%)', 'Others (44.24%)']

wedges, texts = ax.pie(data, wedgeprops=dict(width=0.5), startangle=-40)

bbox_props = dict(boxstyle="square,pad=0.3", fc="w", ec="k", lw=0.72)
kw = dict(arrowprops=dict(arrowstyle="-"),
          bbox=bbox_props, zorder=0, va="center")

for i, p in enumerate(wedges):
    ang = (p.theta2 - p.theta1)/2. + p.theta1
    y = np.sin(np.deg2rad(ang))
    x = np.cos(np.deg2rad(ang))
    horizontalalignment = {-1: "right", 1: "left"}[int(np.sign(x))]
    connectionstyle = "angle,angleA=0,angleB={}".format(ang)
    kw["arrowprops"].update({"connectionstyle": connectionstyle})
    ax.annotate(names[i], xy=(x, y), xytext=(1.1*np.sign(x), 1.2*y),
                horizontalalignment=horizontalalignment, **kw, fontsize=17)

ax.set_title("Biomass function origin in the BiGG database", fontsize=25)
plt.savefig('Comparison_of_biomass_function_origin_pie.png')
plt.show()
