#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Analysis of data from several invasion experiments using the code described in
Invasion2.py. For creating the invasion data necessary for this analysis the 
bash script "Population_reaction_experiment.sh was created"
"""

import os
import pickle
import numpy as np
import matplotlib.pyplot as plt



__author__ = 'Tim Nies'
__credit__='Tim Nies'
__liscence__='GPLv3'
__vesion__='0.1'
__maintainer__='Tim Nies'

#%% Load data 

data_dir = '/home/tim/Desktop/Bacgrowth/Git/ecomodels/Models/data/Experiment_Invasion'
dirs = os.listdir(data_dir)
files = ['Invasion_1000points_40s30r_sameD_metaFlog.p','Invasion_1000points_40s30r_sameD_pop_reaction.p',
         'Invasion_1000points_40s30r_sameD_org_ss.p']
#%% Analyze data

data_dict = {}

for i in dirs:
    if i != '5000': #can be disabled when no 5000 file is in the dir
        tmp_dict = {}
        for j in files:
            tmp_dict[j.split('_')[-1]] = np.array(pickle.load(open(data_dir + '/' + i + '/' + j, "rb" )))
    data_dict[i] = tmp_dict

#%% Plot 

fig,ax = plt.subplots(figsize=(15,12))
for k in data_dict:
    pos = sum(data_dict[k]['reaction.p'] > 0)
    pos_reduced = pos[pos != 0]
    ss = data_dict[k]['ss.p'][:40]
    plt.scatter(ss[pos != 0],pos_reduced, s=500, c='b', alpha=0.6 )  
plt.xlabel('Steady state abundance of a species before invasion', size=25)
plt.ylabel('Number of increases in species abundance after invasion', size=22)
ax.set_title('Relationship between pre-invasion steady state abundance and number of beneficial effects of invasion ', fontsize=20, pad=30)
plt.tick_params(labelsize=25)
plt.savefig('PopReaction2.png')
plt.show()
