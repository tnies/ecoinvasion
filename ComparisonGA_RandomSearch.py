#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Compares computational efficiency between random search and GA
"""


import numpy as np
import matplotlib.pyplot as plt
from numba import jit
from models import Mcrm
from utils import get_number_of_surviving_species
from utils import get_invader_model
from utils import getConsumerMatrix, getMetabolismMatrix
from utils import save_model



__author__ = 'Tim Nies'
__credit__='Tim Nies'
__liscence__='GPLv3'
__vesion__='0.1'
__maintainer__='Tim Nies'


#%% Load the basic model and prepare for invasion

model = Mcrm()
l_invasion = 0.8
n_resources = 30
n_species = 40

#define parameters
model.change_metaparams({'number_species':n_species,'number_resources':n_resources, 
                         'h':'renewable', 'sigma': 'type I'})

number_species = model.metaparams['number_species']
number_resources = model.metaparams['number_resources']
 
model.params['C'] = getConsumerMatrix(model)
model.params["D"] = getMetabolismMatrix(model)
model.params['k'] = np.zeros(number_resources)
model.params['k'][0] = 10e4
model.params['l'] = np.repeat(l_invasion,number_resources)

#make test simulation and plot
y0_resources = np.zeros(number_resources)
y0_species = np.repeat(10e3,number_species)
y0 = np.concatenate((y0_species,y0_resources),axis=None)
time = np.linspace(0,10000,10000) 
res = model.time_course(time,y0)
res = np.array(res)

plt.plot(time,res[:,:number_species])
plt.ylim(10e-5,10e5)
plt.xscale('log')
plt.xlabel('Time', fontsize=20)
plt.yscale('log')
plt.show()

#obtain steady states of population and make visual proof that it is stable
ss = model.steadystate(y0,toleranz=1e-12)
res = model.time_course(time,ss)
res = np.array(res)
plt.plot(time,res)
plt.show()

save_model(model,"model.p")

#%% Create invasion model and test

model2 = get_invader_model(model)
number_species = model2.metaparams['number_species']
number_resources = model2.metaparams['number_resources']

invasion_start = 1
y0 = np.concatenate((ss[:number_species-1],invasion_start,ss[number_species-1:]),axis=None)
time = np.linspace(0,10000,10000) 
res = model2.time_course(time,y0)
res = np.array(res)

plt.plot(time,res[:,:number_species])
plt.xlabel('Time', fontsize=20)
plt.show()

save_model(model2,"model_invasion.p")

###############################################################################
###############################################################################
# RANDOM SEARCH
###############################################################################
###############################################################################

ROUNDS = 100

import time as measure_time 
start = measure_time.time() 

invader_cs = []
meta_invasion_fitness = [-1000]
max_meta_invasion_fitness = [0]
invaded_ss = []
meta_C = []
all_C = []
for _ in range(ROUNDS):
    if _%50 == 0: print(_)
    c_inv = np.random.rand(number_resources)
    model2.params['C'] = np.vstack((model.params['C'],c_inv))
    all_C.append(model2.params['C'])
    invasion_fitness = model2.system(0,y0)[number_species-1]
    #Efficiency test of search procedure
    if invasion_fitness > max(max_meta_invasion_fitness):
        max_meta_invasion_fitness.append(invasion_fitness)
    else:
        max_meta_invasion_fitness.append(max(max_meta_invasion_fitness))
    #Collect successfull invaders
    if invasion_fitness > 0:
        invader_cs.append(c_inv)
        meta_C.append(model2.params['C'])
        meta_invasion_fitness.append(invasion_fitness)
        invaded_ss.append(model2.steadystate(y0,toleranz=1e-12))

end = measure_time.time()
random_search_time = end-start

#%%
        
number_of_surviving_species = get_number_of_surviving_species(invaded_ss,number_species)
plt.plot(meta_invasion_fitness[1:],number_of_surviving_species,'o')
plt.show()


plt.plot(range(ROUNDS),max_meta_invasion_fitness[1:])
plt.show()

###############################################################################
###############################################################################
#RANDOM SEARCH WITH MULTIPROCESSING
###############################################################################
###############################################################################
#%%

import multiprocessing as mp

c_inv_list = np.array([np.random.rand(number_resources) for i in range(15000)])

def get_invasion_fitness(c_inv):
    model2.params['C'] = np.vstack((model.params['C'],c_inv))
    return model2.system(0,y0)[number_species-1]

def get_invasion_steadystate(c_inv):
    model2.params['C'] = np.vstack((model.params['C'],c_inv))
    return model2.steadystate(y0,toleranz=1e-12)

start_2 = measure_time.time()

pool1 = mp.Pool(processes=4)
pool2 = mp.Pool(processes=2)
results = np.array(pool1.map(get_invasion_fitness,c_inv_list))
idx = np.where(results > 0)[0]
ss = pool2.map(get_invasion_steadystate,c_inv_list[idx])

end_2 = measure_time.time()
random_search_mp_time = end_2-start_2 
#%%

plt.plot(results[idx],get_number_of_surviving_species(ss,number_species),'o')
plt.show()    
#%%
###############################################################################
###############################################################################
#GENETIC ALGORITHM
###############################################################################
###############################################################################

DNA_SIZE = 20
POP_SIZE = 10
CROSS_RATE = 0.8
MUTATION_RATE = 0.12 #ehemals 0.09 für 2,3,4
N_GENERATIONS = 80
X_BOUND = [0,1]


def F(x):
    C = model.params["C"]
    C2 = np.vstack((C,x))
    model2.params['C'] = C2
    invasion_fitness = model2.system(0,y0)[number_species-1]
    if invasion_fitness<0:
        return 1e-12
    return invasion_fitness

#Defintion Fitnessfunktion
def get_fitness(pred): return pred - min(pred) + 1e-10

#Binäre DNA soll decimal und auf X_bound normalisiert sein
def translateDNA(pop): return pop.dot(2 ** np.arange(DNA_SIZE)[::-1]) / float(2**DNA_SIZE-1) * X_BOUND[1]

#Definition Selektion
def select(pop, fitness):
    idx = np.random.choice(np.arange(POP_SIZE), size=POP_SIZE, replace = True, p = fitness/fitness.sum())
    return pop[idx]

#Defintion crossover
def crossover(parent, pop):
    if np.random.rand() < CROSS_RATE:
        i_ = np.random.randint(0,POP_SIZE,size=1)
        cross_points = np.random.randint(0,2, size=DNA_SIZE).astype(np.bool)
        parent[cross_points] = pop[i_, cross_points]
    return parent

#Definition Mutation
def mutate(child):
    for point in range(DNA_SIZE):
        if np.random.rand() < MUTATION_RATE:
            child[point] = 1 if child[point] == 0 else 0
    return child



@jit(parallel=True)
def GA_analysis(Meta_pop_size):
    meta_org_C = []
    meta_fitness_log = []
    meta_F_log = []
    meta_matrices = []
    meta_steadystates = []
    pop_reaction = []
    for _ in range(Meta_pop_size):
        #model.params["D"] = [model.getMetabolismMatrix()] #is it in one or in all models ?
        if _%100 == 0:
            print(_)
        pop = np.random.randint(2,size=(1*number_resources,POP_SIZE,DNA_SIZE))
        fitness_log = []
        F_log = []
        for k in range(N_GENERATIONS):
            #Evaluate
            translated = translateDNA(pop)
            matrices = translated.T.reshape(POP_SIZE,1,number_resources)
            F_values = []
            for i in matrices:
                F_values.append(F(i))
            F_log.append(F_values)
            fitness = get_fitness(np.array(F_values))
            fitness_log.append(fitness)
            #best_point = [translateDNA(i[np.argmax(fitness),:]) for i in pop]
    
            #Evolve
            for j in range(len(pop)):
                pop_x = select(pop[j], fitness)
                pop_x_copy = pop_x.copy()
                for parent in pop_x_copy:
                    child = crossover(parent, pop_x_copy)
                    child = mutate(child)
                    parent[:] = child
                    pop[j] = pop_x
        
        steadystates = model2.steadystate(y0)
        meta_org_C.append(model2.params["C"])    
        meta_matrices.append(matrices[0])
        meta_fitness_log.append(fitness_log)
        meta_F_log.append(F_log)
        meta_steadystates.append(steadystates)
        pop_reaction.append(steadystates[:number_species-1] - y0[:number_species-1])
    return pop_reaction, meta_steadystates, meta_org_C, meta_matrices, meta_fitness_log, meta_F_log

start_3 = measure_time.time()

pop_reaction, meta_steadystates, meta_org_C, meta_matrices,meta_fitness_log, meta_F_log = GA_analysis(1000)

end_3 = measure_time.time()
GA_time = end_3-start_3

meta_steadystates = np.array(meta_steadystates)[:,:number_species]  
idx_of_surviving_species = [np.where(i>1)[0] for i in meta_steadystates]  
number_of_surviving_species = np.array([len(i) for i in idx_of_surviving_species])
converged_F_logs = np.round(np.array(meta_F_log)[:,-1,-1],4)

#%%
plt.figure(figsize=(15,12))
plt.scatter(converged_F_logs,number_of_surviving_species)
plt.xlabel("Invasion fitness",fontsize=40)
plt.ylabel("Number of surviving Species",fontsize=40)
plt.xticks(fontsize=40)
plt.yticks(fontsize=40)
plt.show()
